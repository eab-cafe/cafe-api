const mongoose = require('mongoose');

const ResourceListItem = new mongoose.Schema({
  displayId: {
    type: String,
    required: true,
  },
  referenceId: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

module.exports = mongoose.model('Resource_List_Item', ResourceListItem);
// module.exports = mongoose.model('Resource_List_Item', ResourceListItem);
