/**
 * @module controller/question
 */
const Section = require('../models/section');
const Question = require('../models/question');
const responseUtil = require('../utils/response.util');

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Save question information to mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.sectionId - section id which the question belongs to
 * @param {string} req.body.index - index of question of section
 * @param {string} req.body.title - question title
 * @param {string=} req.body.questionType - question type
 * @param {string=} req.body.answers - question answers
 * @param {boolean=} req.body.isDisable - is the question disable
 * @param {boolean=} req.body.isMandatory - is the question mandatory
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns new question record id from mongodb
 */
module.exports.saveQuestion = async (req, res, next) => {
  if (req.body.sectionId && req.body.index && req.body.title) {
    const section = await Section.findOne({ _id: req.body.sectionId });
    if (section) {
      if (/[0-9]+/.test(req.body.index) && req.body.index >= 0 && req.body.index <= section.questions.length) {
        const newQuestion = new Question({
          title: req.body.title,
          questionType: req.body.questionType ? req.body.questionType : '',
          answers: req.body.answers ? req.body.answers : '',
          isDisable: req.body.isDisable ? req.body.isDisable : false,
          isMandatory: req.body.isMandatory ? req.body.isMandatory : false,
          designSetting: req.body.designSetting ? req.body.designSetting : {},
          gridOrder: req.body.gridOrder,
          gridSpan: req.body.gridSpan,
          visibilityRule: req.body.visibilityRule ? req.body.visibilityRule : [],
          validationRule: req.body.validationRule ? req.body.validationRule : [],
          relationRule: req.body.relationRule ? req.body.relationRule : [],
          trackList: req.body.trackList ? req.body.trackList : [],
          rootId: req.body.rootId ? req.body.rootId : null,
          referenceId: req.body.referenceId ? req.body.referenceId : '',

        });
        const id = '_id';
        const newQuestionResult = await newQuestion.save();
        section.questions.splice(req.body.index, 0, newQuestionResult[id]);
        await Section.findOneAndUpdate({
          _id: req.body.sectionId,
        }, {
          questions: section.questions,
        }, {
          new: true,
        });
        responseUtil.generalSuccessResponse(req, res, next, {
          _id: newQuestionResult[id],
        });
      } else {
        responseUtil.generalBadRequestResponse(req, res, next, 'Index parameter error. It should be number and >=0 and <= form.pages.length');
      }
    } else {
      responseUtil.generalBadRequestResponse(req, res, next, `Section ${req.body.sectionId} not exists`);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'sectionId, index, title are required');
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get question information from mongodb
 * @param {object} req - express request param
 * @param {object} req.params - express param body
 * @param {string} req.params.questionId - question id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Question info
 */
module.exports.getQuestion = async (req, res, next) => {
  const questionResult = await Question.findOne({ _id: req.params.questionId });
  if (questionResult) {
    responseUtil.generalSuccessResponse(req, res, next, questionResult);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Question ${req.params.questionId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Update question information
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.questionId - question id pass from client side
 * @param {object} req.body - express request body
 * @param {string} req.body.title - question title
 * @param {string=} req.body.questionType - question type
 * @param {string=} req.body.answers - question answers
 * @param {boolean=} req.body.isDisable - is the question disable
 * @param {boolean=} req.body.isMandatory - is the question mandatory
 * @param {number=} req.body.designSetting - design setting of the question
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns New question info
 */
module.exports.updateQuestion = async (req, res, next) => {
  const result = await Question.findOne({ _id: req.params.questionId });
  if (result && req.body === {}) {
    responseUtil.generalSuccessResponse(req, res, next, result);
  } else if (result) {
    const newQuestionData = {};
    if (req.body.title) {
      newQuestionData.title = req.body.title;
    }
    if (req.body.questionType) {
      newQuestionData.questionType = req.body.questionType;
    }
    if (req.body.answers) {
      newQuestionData.answers = req.body.answers;
    }
    if (req.body.gridOrder) {
      newQuestionData.gridOrder = req.body.gridOrder;
    }
    if (req.body.gridSpan) {
      newQuestionData.gridSpan = req.body.gridSpan;
    }
    const newQuestion = await Question.findOneAndUpdate({
      _id: req.params.questionId,
    }, newQuestionData, {
      new: true,
    });
    responseUtil.generalSuccessResponse(req, res, next, newQuestion);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Question ${req.params.questionId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Delete question
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.pageId - question id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Deleted question id
 */
module.exports.deleteQuestion = async (req, res, next) => {
  const result = await Question.findOne({ _id: req.params.questionId });
  if (result) {
    await Question.findByIdAndDelete({ _id: req.params.questionId });
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: req.params.questionId,
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Question ${req.params.questionId} not exists.`);
  }
};
