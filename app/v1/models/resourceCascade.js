const mongoose = require('mongoose');
// const Resource = require('./resource');

const ResourceCascadeModel = new mongoose.Schema({
  referenceId: {
    type: String,
    required: true,
  },
  title: {
    type: String,
  },
  languageMap: {
    type: Array,
    ref: 'Language',
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  list: {
    type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
    ref: 'Resource_List',
    default: [],
  },
  mapping: {
    type: Array,
    default: [],
  },
});

module.exports = mongoose.model('Resource_Cascade', ResourceCascadeModel);
// Resource.discriminator('Resource_Cascade', ResourceCascadeModel);
