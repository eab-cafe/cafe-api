const mongoose = require('mongoose');

const FormDesignThemeModel = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  designLevel: {
    type: String,
  },
  designSetting: {
    type: Object,
    default: {},
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  createdBy: {
    type: String,
    required: true,
    default: 'admin',
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedBy: {
    type: String,
    required: true,
    default: 'admin',
  },
}, { minimize: false });

module.exports = mongoose.model('FormDesignTheme', FormDesignThemeModel);
