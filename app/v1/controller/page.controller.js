/**
 * @module controller/page
 */
const _ = require('lodash');

const Form = require('../../v2/models/form');
const Page = require('../models/page');
const Section = require('../models/section');
const Question = require('../models/question');

const responseUtil = require('../utils/response.util');

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Save page information to mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.formId - form id which the page belongs to
 * @param {string} req.body.index - index of pages of form
 * @param {string} req.body.title - page title
 * @param {string=} req.body.description - page description
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns new page record id from mongodb
 */
module.exports.savePage = async (req, res, next) => {
  if (req.body.formId && req.body.index && req.body.title) {
    const form = await Form.findOne({ _id: req.body.formId });
    if (form) {
      if (/[0-9]+/.test(req.body.index) && req.body.index >= 0 && req.body.index <= form.pages.length) {
        const newPage = new Page({
          title: req.body.title,
          description: req.body.description ? req.body.description : '',
          sections: [],
          designSetting: req.body.designSetting ? req.body.designSetting : {},
          visibilityRule: req.body.visibilityRule ? req.body.visibilityRule : [],
          referenceId: req.body.referenceId ? req.body.referenceId : '',
        });
        const id = '_id';
        const newPageResult = await newPage.save();
        form.pages.splice(req.body.index, 0, newPageResult[id]);
        await Form.findOneAndUpdate({
          _id: req.body.formId,
        }, {
          pages: form.pages,
        }, {
          new: true,
        });
        responseUtil.generalSuccessResponse(req, res, next, {
          _id: newPageResult[id],
        });
      } else {
        responseUtil.generalBadRequestResponse(req, res, next, 'Index parameter error. It should be number and >=0 and <= form.pages.length');
      }
    } else {
      responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.body.formId} not exists`);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'formId, index, title are required');
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get page information from mongodb
 * @param {object} req - express request param
 * @param {object} req.params - express param body
 * @param {string} req.params.pageId - page id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Page info
 */
module.exports.getPage = async (req, res, next) => {
  const pageResult = await Page.findOne({ _id: req.params.pageId });
  if (pageResult) {
    const getSectionDetailPromiseArr = [];
    _.forEach(pageResult.sections, (sectionId) => {
      getSectionDetailPromiseArr.push(new Promise((resolve, reject) => {
        Section.findOne({ _id: sectionId }, (err, section) => {
          if (err) {
            reject(err);
          } else {
            const getQuestionDetailPromiseArr = [];
            _.forEach(section.questions, (questionId) => {
              getQuestionDetailPromiseArr.push(new Promise(
                (queryQuestionResolve, queryQuestionReject) => {
                  Question.findOne({ _id: questionId }, (queryQuestionErr, questionDetail) => {
                    if (queryQuestionErr) {
                      queryQuestionReject(err);
                    } else {
                      queryQuestionResolve(questionDetail);
                    }
                  });
                },
              ));
            });
            Promise
              .all(getQuestionDetailPromiseArr)
              .then((questions) => {
                const id = '_id';
                const sectionWithQuestions = {
                  _id: section[id],
                  pageId: section.pageId,
                  title: section.title,
                  description: section.description,
                  questions,
                  designSetting: section.designSetting ? section.designSetting : {},
                  columnCount: section.columnCount ? section.columnCount : undefined,
                  visibilityRule: section.visibilityRule ? section.visibilityRule : [],
                  referenceId: section.referenceId ? section.referenceId : '',
                  createdAt: section.createdAt,
                  updatedAt: section.updatedAt,
                };
                resolve(sectionWithQuestions);
              });
          }
        });
      }));
    });
    Promise
      .all(getSectionDetailPromiseArr)
      .then((results) => {
        const id = '_id';
        const version = '_v';
        const pageResultWithSections = {
          _id: pageResult[id],
          title: pageResult.title,
          description: pageResult.description ? pageResult.description : '',
          sections: results,
          designSetting: pageResult.designSetting ? pageResult.designSetting : {},
          referenceId: pageResult.referenceId ? pageResult.referenceId : '',
          createdAt: pageResult.createdAt,
          updatedAt: pageResult.updatedAt,
          _v: pageResult[version],
        };
        responseUtil.generalSuccessResponse(req, res, next, pageResultWithSections);
      })
      .catch((err) => {
        responseUtil.generalInternalServerErrorResponse(req, res, next, err);
      });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Page ${req.params.pageId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Update page information
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.pageId - page id pass from client side
 * @param {object} req.body - express request body
 * @param {string=} req.body.title - new page title
 * @param {string=} req.body.description - new page description
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns New page info
 */
module.exports.updatePage = async (req, res, next) => {
  const result = await Page.findOne({ _id: req.params.pageId });
  if (result && req.body === {}) {
    responseUtil.generalSuccessResponse(req, res, next, result);
  } else if (result) {
    const newPageData = {};
    if (req.body.title) {
      newPageData.title = req.body.title;
    }
    if (req.body.description) {
      newPageData.description = req.body.description;
    }
    const newPage = await Page.findOneAndUpdate({
      _id: req.params.pageId,
    }, newPageData, {
      new: true,
    });
    responseUtil.generalSuccessResponse(req, res, next, newPage);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Page ${req.params.pageId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Delete page
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.pageId - page id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Deleted page id
 */
module.exports.deletePage = async (req, res, next) => {
  const result = await Page.findOne({ _id: req.params.pageId });
  if (result) {
    await Page.findByIdAndDelete({ _id: req.params.pageId });
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: req.params.pageId,
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Page ${req.params.pageId} not exists.`);
  }
};
