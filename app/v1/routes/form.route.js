const express = require('express');

const router = express.Router();

const formController = require('../controller/form.controller');

router.get('/', (req, res, next) => {
  formController.getForms(req, res, next);
});
router.get('/formTemplate', (req, res, next) => {
  formController.getFormTemplates(req, res, next);
});
router.post('/', (req, res, next) => {
  formController.saveForm(req, res, next);
});

router.get('/publishedFormsList/', (req, res, next) => {
  formController.getPublishedFormsList(req, res, next);
});

router.get('/:formId', (req, res, next) => {
  formController.getForm(req, res, next);
});

router.put('/:formId', (req, res, next) => {
  formController.updateForm(req, res, next);
});

router.delete('/:formId', (req, res, next) => {
  formController.deleteForm(req, res, next);
});

router.post('/:formId/publish', (req, res, next) => {
  formController.publishForm(req, res, next);
});

router.get('/:formId/publishList', (req, res, next) => {
  formController.publishFormList(req, res, next);
});

router.get('/publish/:publishId', (req, res, next) => {
  formController.getPublishFormDetail(req, res, next);
});

router.post('/:formId/importDataRules', (req, res, next) => {
  formController.saveImportDataRules(req, res, next);
});

router.put('/:formId/importDataRules', (req, res, next) => {
  formController.updateImportDataRules(req, res, next);
});

router.get('/:formId/language', (req, res, next) => {
  formController.getLang(req, res, next);
});

router.post('/:formId/language', (req, res, next) => {
  formController.saveLang(req, res, next);
});

router.put('/:formId/language', (req, res, next) => {
  formController.updateLang(req, res, next);
});

router.delete('/:formId/language/:langId', (req, res, next) => {
  formController.deleteLang(req, res, next);
});
module.exports = router;
