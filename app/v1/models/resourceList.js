const mongoose = require('mongoose');
// const Resource = require('./resource');
// const ResourceListItem = require('./resourceListItem');

const ResourceListModel = new mongoose.Schema({
  key: {
    type: String,
  },
  title: {
    type: String,
  },
  referenceId: {
    type: String,
    required: true,
  },
  list: {
    type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
    ref: 'Resource_List_Item',
    default: [],
  },
  languageMap: {
    type: Array,
    ref: 'Language',
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  // ? Hide from List
  isHidden: {
    type: Boolean,
    default: false,
  },
});

const ResourceList = mongoose.model('Resource_List', ResourceListModel);

module.exports = ResourceList;
// Resource.discriminator('Resource_List', ResourceListModel);
