const express = require('express');

const router = express.Router();

const pageController = require('../controller/page.controller');

router.post('/', (req, res, next) => {
  pageController.savePage(req, res, next);
});

router.get('/:pageId', (req, res, next) => {
  pageController.getPage(req, res, next);
});

router.put('/:pageId', (req, res, next) => {
  pageController.updatePage(req, res, next);
});

router.delete('/:pageId', (req, res, next) => {
  pageController.deletePage(req, res, next);
});

module.exports = router;
