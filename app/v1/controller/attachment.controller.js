// ! BUG
/* eslint-disable security/detect-non-literal-fs-filename */

/**
 * @module controller/attachment
 */
const mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const fs = require('fs');

const MinIoMapping = require('../models/minIoMapping');

const log4jUtil = require('../utils/log4j.util');
const responseUtil = require('../utils/response.util');
const minIoUtil = require('../utils/minIo.util');

const { connection, mongo } = mongoose;

module.exports.saveAttachment = async (req, res, next) => {
  if (req.file && req.file.filename) {
    try {
      const gfs = new Grid(connection.db, mongo);
      const writeStream = gfs.createWriteStream({
        filename: req.file.filename,
        root: 'attachment',
      });
      fs.createReadStream(req.file.path).pipe(writeStream);
      writeStream.on('close', (file) => {
        responseUtil.generalSuccessResponse(req, res, next, file);
      });
      writeStream.on('error', (error) => {
        log4jUtil.log('warn', `An Error ocurred: ${error}`);
        throw error;
      });
    } catch (e) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, e);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'file is required.');
  }
};

module.exports.getAttachment = async (req, res, next) => {
  if (req.query.fileId) {
    const gfs = new Grid(connection.db, mongo);
    try {
      const name = req.query.fileId;
      const readStream = gfs.createReadStream({
        filename: name,
        root: 'attachment',
      });
      res.writeHead(200, { 'Content-Type': 'image/jpeg' });
      readStream.pipe(res);

      readStream.on('error', (error) => {
        log4jUtil.log('warn', `An Error ocurred: ${error}`);
        throw error;
      });
    } catch (e) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, e);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'fileId is required.');
  }
};

/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description upload file to minIo by id
 * @param  {string} imageId - minIo server file id
 * @param  {file}  - upload file
 */
module.exports.minIoUploadFile = async (req, res, next) => {
  if (req.file) {
    try {
      const result = {};
      const fileStream = await fs.createReadStream(req.file.path);
      const uploadResult = await minIoUtil.uploadFileThroughFileApi(
        req.params.imageId,
        req.file.originalname,
        fileStream,
      );
      fs.unlinkSync(`${process.env.PWD}/${req.file.path}`);
      log4jUtil.log('info', 'Temp file deleted');
      if (uploadResult.error) {
        log4jUtil.log('error', JSON.stringify(uploadResult.error));
        responseUtil.generalInternalServerErrorResponse(req, res, next, uploadResult.error);
      } else {
        const fileUrl = await minIoUtil.getFileUrlThroughFileApi(req.params.imageId);
        result.id = req.params.imageId;
        result.url = fileUrl;
        responseUtil.generalSuccessResponse(req, res, next, result);
      }
    } catch (e) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, e);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'file is required.');
  }
};

/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description get minIo file by id
 * @param  {string} imageId - minIo server file id
 */
module.exports.getMinIoUploadFile = async (req, res, next) => {
  if (req.params.imageId) {
    try {
      const result = {};
      const fileUrl = await minIoUtil.getFileUrlThroughFileApi(req.params.imageId);
      result.id = req.params.imageId;
      result.url = fileUrl;
      responseUtil.generalSuccessResponse(req, res, next, result);
    } catch (e) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, e);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'file id is required.');
  }
};
/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description delete minIo file by id
 * @param  {string} imageId - minIo server file id
 */
module.exports.deleteMinIoUploadFile = async (req, res, next) => {
  if (req.params.imageId) {
    try {
      const result = {};
      const cnt = await MinIoMapping.countDocuments(
        {
          mappingFormId: { $ne: req.params.formId },
          mappingId: req.params.imageId,
        },
      );
      if (cnt === 0) {
        await minIoUtil.deleteFileUrlThroughFileApi(req.params.imageId);
      }
      // result.id = req.params.imageId;
      // result.url = fileUrl;
      responseUtil.generalSuccessResponse(req, res, next, result);
    } catch (e) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, e);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'file id is required.');
  }
};
