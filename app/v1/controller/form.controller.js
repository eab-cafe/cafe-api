/**
 * @module controller/form
 */
const _ = require('lodash');
const moment = require('moment');

const Form = require('../../v2/models/form');
const Language = require('../models/language');
const FormPublish = require('../models/formPublish');
const Page = require('../models/page');
const Section = require('../models/section');
const Question = require('../models/question');
const MinIoMapping = require('../models/minIoMapping');
// const FormDesignTheme = require('../models/formDesignTheme');


const validateUtil = require('../utils/validator.util');
const responseUtil = require('../utils/response.util');
const log4jUtil = require('../utils/log4j.util');
const formUtil = require('../utils/form.util');
const commonUtil = require('../utils/common.util');

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @constant
 * @type {string}
 * @description In case of eslint [disallow dangling underscores in identifiers]{@link https://eslint.org/docs/rules/no-underscore-dangle} issue, we define
 * variable id = '_id', then use object[id] instead of object._id
 */
const id = '_id';
const formTemplate = 'formTemplate';

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Create new form
 * @param {object} req - express request param
 * @param {object} req.body - body of express request param
 * @param {string} req.body.title - new form title(required)
 * @param {string=} req.body.description - new form description
 * @param {string=} req.body.icon - new form icon url
 * @param {object=} req.body.designSetting - new form design setting
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns {string} new form id from mongodb
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/post_form}
 */
module.exports.saveForm = async (req, res, next) => {
  const newForm = new Form({
    title: req.body.title,
    description: req.body.description ? req.body.description : '',
    icon: req.body.icon ? req.body.icon : '',
    designSetting: req.body.designSetting ? req.body.designSetting : {},
    languageSetting: req.body.languageSetting ? req.body.languageSetting : {},
  });
  try {
    const result = await newForm.save();
    responseUtil.generalSuccessResponse(req, res, next, result[id]);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

const getPulbishItems = formId => new Promise((resolve, reject) => {
  FormPublish
    .find({ ownerFormId: formId }, { _id: 1, createdAt: 1 }, { lean: true })
    .sort('-createdAt')
    .exec((err, doc) => {
      if (err) {
        reject(err);
      } else {
        resolve(doc);
      }
    });
});

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get a set of records by mongoose model and mongodb ids
 * @param  {Form|Page|Section|Question} Model - Mongoose model
 * @param  {array} recordIds - form ids/page ids/section ids/question ids
 * @returns {object} array of Form|Page|Section|Question results
 */
const getRecords = (Model, recordIds) => {
  const promiseArr = [];
  _.forEach(recordIds, (record) => {
    promiseArr.push(new Promise((resolve, reject) => {
      Model.findById(record, null, { lean: true }, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    }));
  });
  return Promise.all(promiseArr);
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get a set of sections from db by pages object
 * @param  {array} pages - pages object in form object
 * @returns array of object which is including page index and section records from db
 */
const getSectionsInPages = async (pages) => {
  const sectionPromises = [];
  const tempPages = _.cloneDeep(pages);
  _.forEach(pages, (page, index) => {
    sectionPromises.push(new Promise((resolve) => {
      getRecords(Section, page.sections).then((sectionsResult) => {
        tempPages[index].sections = sectionsResult;
        resolve({
          pageIndex: index,
          records: sectionsResult,
        });
      });
    }));
  });
  return Promise.all(sectionPromises);
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get a set of questions from db by pages object
 * @param  {array} pages - pages object in form object
 * @returns array of object which is including page index,
 * section index and question records from db
 */
const getQuestionsInSections = async (pages) => {
  const questionPromises = [];
  _.forEach(pages, (page, pageIndex) => {
    _.forEach(page.sections, (section, sectionIndex) => {
      if (section.questions.length > 0) {
        questionPromises.push(new Promise((resolve) => {
          getRecords(Question, section.questions).then((questionsResult) => {
            resolve({
              pageIndex,
              sectionIndex,
              records: questionsResult,
            });
          });
        }));
      }
    });
  });
  return Promise.all(questionPromises);
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description After get form record from db, we need get more details
 * with pages, sections, questions. Firstly, this function extracts page ids of form
 * and then get all pages records from dbs. Secondly, it extracts section ids from
 * each page and then get all section records from dbs. Thirdly, it extracts question
 * ids from each section and then get all question records from dbs. Finally, return
 * the result just like the [spec format]{@link
 * http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form__formId_}
 * @param {object} form - form object from db
 * @param {string} form._id - mongodb unique id of form object
 * @param {string} form.title - title of form object
 * @param {string=} form.description - description of form object
 * @param {string} form.icon - icon url of form object
 * @param {array} form.pages - page ids of form object
 * @param {date} form.createdAt - created date of form object
 * @param {date} form.updatedAt - updated date of form object
 * @returns {object} form info with its page info, section info and question info
 */
const getFormDetail = async (form) => {
  const tempForm = _.cloneDeep(form);
  // Get pages
  tempForm.pages = await getRecords(Page, tempForm.pages);
  // Get sections
  const pageWithSectionsResult = await getSectionsInPages(tempForm.pages);
  _.forEach(pageWithSectionsResult, (pageWithSections) => {
    tempForm.pages[pageWithSections.pageIndex].sections = pageWithSections.records;
  });
  // Get questions
  const pageWithQuestionsResult = await getQuestionsInSections(tempForm.pages);
  _.forEach(pageWithQuestionsResult, (pageWithQuestion) => {
    tempForm
      .pages[pageWithQuestion.pageIndex]
      .sections[pageWithQuestion.sectionIndex].questions = pageWithQuestion.records;
  });
  const result = await getPulbishItems(form[id]);
  if (result) {
    tempForm.formPublishList = result;
  }
  return tempForm;
};

const deleteLanguageFunc = async (langId) => {
  try {
    const foundLanguage = await Language.findOne({ _id: langId });
    if (!foundLanguage) {
      return false;
    }
    const deleteResult = Language.findOneAndDelete({ _id: langId });
    return deleteResult || true;
  } catch (err) {
    return false;
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get form data
 * @param {object} req - express request param
 * @param {object} req.params - params of express request param
 * @param {string} req.params.formId - formId from url
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form__formId_}
 */
module.exports.getForm = async (req, res, next) => {
  const result = await Form
    .findOne({ _id: req.params.formId }, null, { lean: true })
    .catch(err => ({ err }));
  if (result && !result.err) {
    const formDetail = await getFormDetail(result);
    responseUtil.generalSuccessResponse(req, res, next, formDetail);
  } else {
    log4jUtil.log('warn', `Form not exist. FormId: ${req.params.formId}`);
    responseUtil.generalBadRequestResponse(req, res, next, result);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get a set of form data
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form}
 */
module.exports.getForms = async (req, res, next) => {
  try {
    const results = await Form.find(
      { templateType: { $ne: formTemplate } },
    ).catch(err => ({ error: err }));
    responseUtil.generalSuccessResponse(req, res, next, results);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};


/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description Get a set of formTemplate data
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form}
 */
module.exports.getFormTemplates = async (req, res, next) => {
  try {
    const results = await Form.find(
      { templateType: formTemplate },
    ).catch(err => ({ error: err }));
    responseUtil.generalSuccessResponse(req, res, next, results);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description find created pages from front end
 * @param  {object} pages - pages object of form object
 * @returns array of page records with type, page index and record
 */
const findNewPageRecords = (pages) => {
  const newPages = [];
  if (pages && pages.length > 0) {
    _.forEach(pages, (page, pageIndex) => {
      if (!page[id] || page[id].indexOf('_P') > -1) {
        newPages.push({
          type: 'page',
          pageIndex,
          record: page,
        });
      }
    });
  }
  return newPages;
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description find created sections from front end
 * @param  {object} pages - pages object of form object
 * @returns array of section records with type, page index, section index and record
 */
const findNewSectionRecords = (pages) => {
  const newSections = [];
  if (pages && pages.length > 0) {
    _.forEach(pages, (page, pageIndex) => {
      if (page.sections && page.sections.length > 0) {
        _.forEach(page.sections, (section, sectionIndex) => {
          if (!section[id] || section[id].indexOf('_S') > -1) {
            newSections.push({
              type: 'section',
              pageIndex,
              sectionIndex,
              record: section,
            });
          }
        });
      }
    });
  }
  return newSections;
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description find created questions from front end
 * @param  {object} pages - pages object of form object
 * @returns array of question records with type, page index, section index, question id and record
 */
const findNewQuestionRecords = (pages) => {
  const newQuestions = [];
  if (pages && pages.length > 0) {
    _.forEach(pages, (page, pageIndex) => {
      if (page.sections && page.sections.length > 0) {
        _.forEach(page.sections, (section, sectionIndex) => {
          if (section.questions && section.questions.length > 0) {
            _.forEach(section.questions, (question, questionIndex) => {
              if (!question[id] || question[id].indexOf('_Q') > -1) {
                newQuestions.push({
                  type: 'question',
                  pageIndex,
                  sectionIndex,
                  questionIndex,
                  record: question,
                });
              }
            });
          }
        });
      }
    });
  }
  return newQuestions;
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @type aaa
 * @description When front end create new page, section or question,
 * we will find them from pages object
 * @param  {array} pages - pages object of form
 * @returns array of new created records from front end
 */
const findNewRecords = (pages) => {
  const newRecords = [];
  newRecords.push(...findNewPageRecords(pages));
  newRecords.push(...findNewSectionRecords(pages));
  newRecords.push(...findNewQuestionRecords(pages));
  return newRecords;
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Create new records (includes pages, sections and questions)
 * @param {array} newRecords - newRecords found by last step
 * @returns array of new record created on mongodb with type, index and record
 */
const createNewRecords = async (newRecords) => {
  const tempNewRecords = _.cloneDeep(newRecords);
  const promises = [];
  _.forEach(tempNewRecords, (newRecord) => {
    if (newRecord.type === 'page') {
      const newPage = new Page(_.omit(newRecord.record, ['_id']));
      promises.push(new Promise((resolve, reject) => {
        newPage.save((err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({
              type: 'page',
              pageIndex: newRecord.pageIndex,
              result,
            });
          }
        });
      }));
    } else if (newRecord.type === 'section') {
      const newSection = new Section(_.omit(newRecord.record, ['_id']));
      promises.push(new Promise((resolve, reject) => {
        newSection.save((err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({
              type: 'section',
              pageIndex: newRecord.pageIndex,
              sectionIndex: newRecord.sectionIndex,
              result,
            });
          }
        });
      }));
    } else {
      const newQuestion = new Question(_.omit(newRecord.record, ['_id']));
      promises.push(new Promise((resolve, reject) => {
        newQuestion.save((err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve({
              type: 'question',
              pageIndex: newRecord.pageIndex,
              sectionIndex: newRecord.sectionIndex,
              questionIndex: newRecord.questionIndex,
              result,
            });
          }
        });
      }));
    }
  });
  const newRecordsWithId = await Promise.all(promises);
  return newRecordsWithId;
};
/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @param  {object} reqBody - request body from express framework
 * @param  {array} createdNewRecords - created new records from last step
 * @returns new form with finally pages, sections and questions
 */
const createNewForm = (reqBody, createdNewRecords) => {
  const tempForm = _.cloneDeep(reqBody);
  tempForm.updatedAt = moment();
  _.forEach(createdNewRecords, (createdRecord) => {
    if (createdRecord.type === 'page') {
      tempForm.pages[createdRecord.pageIndex][id] = createdRecord.result[id].toString();
    } else if (createdRecord.type === 'section') {
      tempForm
        .pages[createdRecord.pageIndex]
        .sections[createdRecord.sectionIndex][id] = createdRecord.result[id].toString();
    } else {
      tempForm
        .pages[createdRecord.pageIndex]
        .sections[createdRecord.sectionIndex]
        .questions[createdRecord.questionIndex][id] = createdRecord.result[id].toString();
    }
  });
  return tempForm;
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description update pages field of the form
 * @param  {string} formId - The form id
 * @param  {object} form - New form object from client side
 */
const updateFormWithPageIds = async (formId, form) => Form.findByIdAndUpdate(formId,
  _.omit(form, '_id'),
  //   {
  //   title: form.title ? form.title : '',
  //   description: form.description ? form.description : '',
  //   icon: form.icon ? form.icon : '',
  //   pages: _.map(form.pages, '_id'),
  //   designSetting: form.designSetting ? form.designSetting : {},
  //   updatedAt: moment(),
  // }
  (err, result) => {
    if (err) {
      log4jUtil.log('error', err);
    }
    JSON.stringify(result);
    log4jUtil.log('info', 'success');
  });

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description update sections field of all pages
 * @param  {object} form - New form object from client side
 */
const updatePageWithSectionIds = (form) => {
  _.forEach(form.pages, (page) => {
    Page.findByIdAndUpdate(page[id], {
      displayId: page.displayId ? page.displayId : '',
      title: page.title ? page.title : '',
      description: page.description ? page.description : '',
      sections: _.map(page.sections, '_id'),
      designSetting: page.designSetting ? page.designSetting : {},
      visibilityRule: page.visibilityRule ? page.visibilityRule : [],
      referenceId: page.referenceId ? page.referenceId : '',
    }, (err, result) => {
      if (err) {
        log4jUtil.log('error', err);
      } else {
        JSON.stringify(result);
        log4jUtil.log('info', 'success');
      }
    });
  });
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description update questions field of all sections
 * @param  {object} form - New form object from client side
 */
const updateSectionWithQuestionIds = (form) => {
  _.forEach(form.pages, (page) => {
    _.forEach(page.sections, (section) => {
      Section.findByIdAndUpdate(section[id], {
        displayId: section.displayId ? section.displayId : '',
        title: section.title ? section.title : '',
        description: section.description ? section.description : '',
        questions: _.map(section.questions, '_id'),
        designSetting: section.designSetting ? section.designSetting : {},
        columnCount: section.columnCount ? section.columnCount : undefined,
        visibilityRule: section.visibilityRule ? section.visibilityRule : [],
        referenceId: section.referenceId ? section.referenceId : '',
      }, (err, result) => {
        if (err) {
          log4jUtil.log('error', err);
        } else {
          JSON.stringify(result);
          log4jUtil.log('info', 'success');
        }
      });
    });
  });
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description update all questions detail
 * @param  {object} form - New form object from client side
 */
const updateQuestion = (form) => {
  _.forEach(form.pages, (page) => {
    _.forEach(page.sections, (section) => {
      _.forEach(section.questions, (question) => {
        Question.findByIdAndUpdate(question[id], question, (err, result) => {
          if (err) {
            log4jUtil.log('error', err);
          } else {
            JSON.stringify(result);
            log4jUtil.log('info', 'success');
          }
        }, (err, result) => {
          if (err) {
            log4jUtil.log('error', err);
          } else {
            JSON.stringify(result);
            log4jUtil.log('info', 'success');
          }
        });
      });
    });
  });
};

/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description update all section Background Image by minIo mapping table
 * @param  {object} form - New form object from client side
 */
const updateBackgroundImageMapping = (form, mappingLevel) => {
  MinIoMapping.remove({ mappingFormId: form[id], mappingLevel }, (err) => {
    if (err) log4jUtil.log(err);
    const temp = {
      fileServiceName: commonUtil.getConfig('MinIO_FILE_SERVICE_NAME'),
      bucketName: commonUtil.getConfig('MinIO_BUCKET_NAME'),
      mappingLevel,
      mappingFormId: form[id],
      mappingId: '',
    };

    const defBg = _.get(form, 'designSetting.sectionStyle.header.backgroundImage', '');
    if (defBg !== '') {
      const newMapping = _.cloneDeep(temp);
      newMapping.mappingId = defBg;
      MinIoMapping.findOneAndUpdate(
        { mappingFormId: form[id], mappingId: defBg },
        newMapping, { upsert: true }, (err1) => {
          if (err1) log4jUtil.log(err1);
        },
      );
    }
    _.forEach(form.pages, (page) => {
      _.forEach(page.sections, (section) => {
        const sectionImgID = _.get(section, 'designSetting.header.backgroundImage', '');
        if (sectionImgID !== '') {
          const newMapping = _.cloneDeep(temp);
          newMapping.mappingId = sectionImgID;
          MinIoMapping.findOneAndUpdate({ mappingId: sectionImgID }, newMapping,
            { upsert: true }, (err2) => {
              if (err2) log4jUtil.log(err2);
            });
        }
      });
    });
  });
};
/**
 * xh test add Design Theme
Cafe Default
Eab
Blue
Avocado
 */
// const updateDesignTheme = (form) => {
//   const title = _.get(form, 'title', '');
//   if (title === 'Eab' || title === 'Cafe Default'
//   || title === 'Blue' || title === 'Avocado') {
//     FormDesignTheme.remove({ title, designLevel: 'FORM' }, (err) => {
//       if (err) log4jUtil.log(err);
//       const temp = {
//         title,
//         description: _.get(form, 'description', ''),
//         designLevel: 'FORM',
//         designSetting: _.get(form, 'designSetting', {}),
//       };
//       const newFormDesignTheme = new FormDesignTheme(temp);
//       newFormDesignTheme.save();
//     });
//   }
// };
/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Update form data with page, section, question detail
 * @param {object} req - express request param
 * @param {object} req.params - params of express request param
 * @param {string} req.params.formId - formId from url
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/put_form__formId_}
 */
module.exports.updateForm = async (req, res, next) => {
  const form = await Form.findOne({ _id: req.params.formId }, null, { lean: true });
  if (!form) {
    responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.params.formId} not exists`);
    return;
  }
  const existsNameForm = await Form.findOne({
    _id: { $ne: req.params.formId },
    title: req.body.title,
  }, null, { lean: true });
  if (existsNameForm) {
    responseUtil.generalBadRequestResponse(req, res, next, `Form Name ${req.body.title} is exists`);
    return;
  }
  // Language
  const hasLanguageList = _.get(req.body, 'languageSetting.list', false);
  if (hasLanguageList) {
    const oldFormLanguageList = form.languageSetting.list;
    const newFormLanguageList = req.body.languageSetting.list;
    const removeLanguageList = _.filter(oldFormLanguageList,
      lang => newFormLanguageList.indexOf(lang) < 0);
    _.each(removeLanguageList, async (langItem) => {
      await deleteLanguageFunc(_.get(langItem, '_id'));
      // await Language.findOne({ _id: _.get(langItem, '_id') }).findOneAndDelete();
    });
  }
  // Form
  if (req.body.pages && req.body.pages.length > 0) {
    const newRecords = findNewRecords(req.body.pages);
    const newRecordsWithId = await createNewRecords(newRecords);
    const newForm = createNewForm(req.body, newRecordsWithId);
    // Return to client side with new format and then update db in case of time out
    responseUtil.generalSuccessResponse(req, res, next, newForm);
    updateFormWithPageIds(req.params.formId, newForm);
    updatePageWithSectionIds(newForm);
    updateSectionWithQuestionIds(newForm);
    updateQuestion(newForm);
    updateBackgroundImageMapping(newForm, 'Cafe_Section');
    // updateDesignTheme(newForm);
  } else {
    // Update Form
    const newForm = createNewForm(req.body);
    updateFormWithPageIds(req.params.formId, newForm);
    responseUtil.generalSuccessResponse(req, res, next, newForm);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Delete form data
 * @param {object} req - express request param
 * @param {object} req.params - params of express request param
 * @param {string} req.params.formId - formId from url
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns deleted form id
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/delete_form__formId_}
 */
module.exports.deleteForm = async (req, res, next) => {
  const result = await Form.findOne({ _id: req.params.formId });
  if (result) {
    try {
      await Language.deleteMany({ _id: req.params.formId });
      await Form.findOneAndDelete({ _id: req.params.formId });
      responseUtil.generalSuccessResponse(req, res, next, {
        _id: req.params.formId,
      });
    } catch (err) {
      responseUtil.generalInternalServerErrorResponse(req, res, next, err);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.params.formId} not exists`);
  }
};

module.exports.publishForm = async (req, res, next) => {
  if (req.query.isCreate === 'Y' || req.query.isCreate === 'N') {
    const result = await Form.findOne({ _id: req.params.formId }, null, { lean: true });
    if (result) {
      const formDetail = await getFormDetail(result);
      const formDetailWithoutId = _.omit(formDetail, ['_id']);
      formDetailWithoutId.ownerFormId = req.params.formId;
      const previousPublishedForm = await FormPublish.findOne({ ownerFormId: req.params.formId }).sort('-createdAt');
      if (req.query.isCreate === 'Y' || !previousPublishedForm) {
        formDetailWithoutId.createdAt = moment();
        formDetailWithoutId.updatedAt = moment();
        const newFormPublish = new FormPublish(formDetailWithoutId);
        const savedNewFormPublish = await newFormPublish.save();
        const resultlist = await getPulbishItems(req.params.formId);
        responseUtil.generalSuccessResponse(req, res, next,
          { publishId: savedNewFormPublish[id], formPublishList: resultlist });
        updateBackgroundImageMapping(savedNewFormPublish, 'Cafe_PublishedForm_Section');
      } else {
        formDetailWithoutId.updatedAt = moment();
        const savedNewFormPublish = await FormPublish.findByIdAndUpdate(previousPublishedForm[id],
          formDetailWithoutId);
        const resultlist = await getPulbishItems(req.params.formId);
        responseUtil.generalSuccessResponse(
          req, res, next, { publishId: previousPublishedForm[id], formPublishList: resultlist },
        );
        updateBackgroundImageMapping(savedNewFormPublish, 'Cafe_PublishedForm_Section');
      }
    } else {
      responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.params.formId} not exists`);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `isCreate : ${req.query.isCreate} is error`);
  }
};

module.exports.publishFormList = async (req, res, next) => {
  const result = await getPulbishItems(req.params.formId);
  if (result) {
    responseUtil.generalSuccessResponse(req, res, next, { formPublishList: result });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.params.formId} not exists`);
  }
};

module.exports.getPublishFormDetail = async (req, res, next) => {
  const result = await FormPublish.findById(req.params.publishId)
    .catch(error => ({ error }));
  if (_.has(result, 'error')) {
    responseUtil.generalBadRequestResponse(req, res, next, result.error);
  } else {
    responseUtil.generalSuccessResponse(req, res, next, result);
  }
};

module.exports.saveImportDataRules = async (req, res, next) => {
  const { params: { formId }, body: { importDataRules } } = req;

  // If empty body
  if (_.isEmpty(importDataRules)) {
    responseUtil.generalBadRequestResponse(req, res, next, 'Required fields is Empty');
    return;
  }
  const errorRuleList = validateUtil.checkRuleType(importDataRules);
  if (!_.isEmpty(errorRuleList)) {
    const errorRefIdList = _.map(errorRuleList, error => error.referenceId).join(', ');
    const errorTypeList = _.map(errorRuleList, error => error.importType).join(', ');
    responseUtil.generalBadRequestResponse(req, res, next, `RefID(s) [${errorRefIdList}] using unhandled type(s) [${errorTypeList}] instead of accepted types`);
    return;
  }
  try {
    const form = await Form.findOne({ _id: formId }, null, { lean: true });
    // If no form
    if (!form) {
      responseUtil.generalBadRequestResponse(req, res, next, `Form ${req.params.formId} not exists`);
      return;
    }
    const allFormItemsInRefId = _.map(formUtil.getFlattenFormItems(form), item => item.referenceId);
    const duplicateRuleList = _.filter(importDataRules,
      ({ referenceId }) => !validateUtil.checkUniqueness(allFormItemsInRefId, referenceId));
    if (!_.isEmpty(duplicateRuleList)) {
      const errorRefIdList = _.map(duplicateRuleList, ({ referenceId }) => referenceId);
      responseUtil.generalBadRequestResponse(req, res, next, `RefID(s) [${errorRefIdList.join(', ')}] are duplicated with the existing referenceIds`);
      return;
    }
    const formDetail = await getFormDetail(form);
    // Rewrite new contents
    const newForm = _.cloneDeep(formDetail);
    _.setWith(newForm, 'importDataRules', importDataRules, typeof Array);
    await updateFormWithPageIds(formId, newForm);
    // If written, send success
    responseUtil.generalSuccessResponse(req, res, next, newForm);
  } catch (error) {
    log4jUtil.log('error', error);
    responseUtil.generalInternalServerErrorResponse(req, res, next, error);
  }
};

module.exports.updateImportDataRules = async (req, res, next) => {
  await this.saveImportDataRules(req, res, next);
};

module.exports.getLang = async (req, res, next) => {
  const { query: { code }, params: { formId } } = req;
  try {
    const form = await Form.findOne({ _id: formId }, null, { lean: true });
    if (!form) {
      responseUtil.generalBadRequestResponse(req, res, next, `Form ${formId} not exists`);
      return;
    }
    if (!code) {
      const languages = await Language.find({ formId }, null, { lean: true });
      if (!languages) {
        responseUtil.generalBadRequestResponse(req, res, next, `No language mappings found for formId ${formId}`);
        return;
      }
      responseUtil.generalSuccessResponse(req, res, next, languages);
      return;
    }
    const language = await Language.findOne({ formId, lang: code }, null, { lean: true });
    if (!language) {
      responseUtil.generalBadRequestResponse(req, res, next, `Language mapping of ${code} not exists for formId ${formId}`);
      return;
    }
  } catch (error) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, error);
  }
};

module.exports.saveLang = async (req, res, next) => {
  const { body: { code, set }, params: { formId } } = req;
  const form = await Form.findOne({ _id: formId }, null, { lean: true });
  if (!form) {
    responseUtil.generalBadRequestResponse(req, res, next, `Form ${formId} not exists`);
    return;
  }
  const newLanguage = new Language({
    formId,
    set,
    lang: code,
  });
  const newLanguageResult = await newLanguage.save();
  responseUtil.generalSuccessResponse(req, res, next, newLanguageResult);
};

module.exports.updateLang = async (req, res, next) => {
  const { body: { code, set }, params: { formId } } = req;
  const form = await Form.findOne({ _id: formId }, null, { lean: true });
  if (!form) {
    responseUtil.generalBadRequestResponse(req, res, next, `Form ${formId} not exists`);
    return;
  }
  const updateLanguageResult = await Language.findOneAndUpdate({
    formId,
    lang: code,
  }, {
    set,
  }, {
    new: true,
  });
  responseUtil.generalSuccessResponse(req, res, next, updateLanguageResult);
};

module.exports.deleteLang = async (req, res, next) => {
  const { langId } = req.params;
  const deleteResult = deleteLanguageFunc(langId);
  if (deleteResult) {
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: langId,
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Language ${langId} not exists`);
  }
  // responseUtil.generalInternalServerErrorResponse(req, res, next, err);
};

module.exports.getPublishedFormsList = async (req, res, next) => {
  const result = await Form.find({}, {
    _id: 1,
    title: 1,
    description: 1,
    updatedAt: 1,
  }).catch(err => ({ err }));
  if (result && !result.err) {
    responseUtil.generalSuccessResponse(req, res, next, result);
  } else {
    log4jUtil.log('warn', 'Published Forms List for failure');
    responseUtil.generalBadRequestResponse(req, res, next, result);
  }
};
