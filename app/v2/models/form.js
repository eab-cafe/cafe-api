const mongoose = require('mongoose');
const SchemaConstant = require('../constants/schema');

const FormSchema = new mongoose.Schema({
  title: {
    type: String,
    // required: true,
  },
  description: {
    type: String,
  },
  icon: {
    type: String,
  },
  importDataRuleStatus: {
    type: String,
  },
  importDataRules: [{
    displayId: {
      type: String,
      required: true,
    },
    referenceId: {
      type: String,
      required: true,
    },
    importType: {
      type: String,
      required: true,
    },
    format: {
      type: String,
    },
  }],
  languageSetting: {
    isActivted: {
      type: Boolean,
    },
    list: [{
      code: {
        type: String,
        required: true,
      },
      label: {
        type: String,
      },
    }],
  },
  pages: {
    type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
    ref: 'Page',
    default: [],
  },
  designSetting: {
    type: Object,
    default: {},
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  // ? Hide from List
  isHidden: {
    type: Boolean,
    default: false,
  },
  setting: {
    type: Object,
    default: {},
  },
  templateType: {
    type: String,
    default: 'customize',
  },
  templateGroup: {
    type: String,
  },
}, { minimize: false });

async function getForms(templateType = 'templateType') {
  const result = await this.model(SchemaConstant.FORM)
    .find({ templateType: { $ne: templateType } });
  return result;
}

async function getForm(formId) {
  const result = await this.model(SchemaConstant.FORM).findOne({ _id: formId });
  return result;
}

FormSchema.method('getForms', getForms);
FormSchema.method('getFormById', getForm);

const formModel = mongoose.model(SchemaConstant.FORM, FormSchema);

module.exports = formModel;
