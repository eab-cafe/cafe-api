// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-ADMIN
// ! LOCATION: constants/answerType.js

const TEXT = 'text';
const ARRAY = 'array';
const ARRAYOBJECT = 'array_object';
const ARRAYQUESTION = 'array_question';
const OBJECT = 'object';

/** DATE */
const START_DATE = 'startDate';
const END_DATE = 'endDate';
const START_TIME = 'startTime';
const END_TIME = 'endTime';


module.exports = {
  TEXT,
  ARRAY,
  ARRAYOBJECT,
  ARRAYQUESTION,
  OBJECT,
  START_DATE,
  END_DATE,
  START_TIME,
  END_TIME,
};
