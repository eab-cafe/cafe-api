/**
 * @deprecated use V2 Form Model instead
 * @author felix.chum <felix.chum@eabsystems.com>
 */
// const mongoose = require('mongoose');

// const FormModel = new mongoose.Schema({
//   title: {
//     type: String,
//     // required: true,
//   },
//   description: {
//     type: String,
//   },
//   icon: {
//     type: String,
//   },
//   importDataRuleStatus: {
//     type: String,
//   },
//   importDataRules: [{
//     displayId: {
//       type: String,
//       required: true,
//     },
//     referenceId: {
//       type: String,
//       required: true,
//     },
//     importType: {
//       type: String,
//       required: true,
//     },
//     format: {
//       type: String,
//     },
//   }],
//   languageSetting: {
//     isActivted: {
//       type: Boolean,
//     },
//     list: [{
//       code: {
//         type: String,
//         required: true,
//       },
//       label: {
//         type: String,
//       },
//     }],
//   },
//   pages: {
//     type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
//     ref: 'Page',
//     default: [],
//   },
//   designSetting: {
//     type: Object,
//     default: {},
//   },
//   createdAt: {
//     type: Date,
//     default: Date.now,
//     required: true,
//   },
//   updatedAt: {
//     type: Date,
//     default: Date.now,
//     required: true,
//   },
//   // ? Hide from List
//   isHidden: {
//     type: Boolean,
//     default: false,
//   },
//   setting: {
//     type: Object,
//     default: {},
//   },
//   templateType: {
//     type: String,
//     default: 'customize',
//   },
//   templateGroup: {
//     type: String,
//   },
// }, { minimize: false });

// module.exports = mongoose.model('Form', FormModel);
