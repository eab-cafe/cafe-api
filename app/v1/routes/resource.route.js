const express = require('express');

const router = express.Router();

const resourceController = require('../controller/resource.controller');

router.get('/', (req, res, next) => {
  resourceController.getResources(req, res, next);
});

router.post('/', (req, res, next) => {
  resourceController.saveResource(req, res, next);
});

router.put('/:resourceId', (req, res, next) => {
  resourceController.updateResource(req, res, next);
});

router.delete('/:resourceId', (req, res, next) => {
  resourceController.deleteResource(req, res, next);
});

router.get('/:resourceId', (req, res, next) => {
  resourceController.getResource(req, res, next);
});

router.get('/:resourceId/:resourceType', (req, res, next) => {
  resourceController.getResource(req, res, next);
});


module.exports = router;
