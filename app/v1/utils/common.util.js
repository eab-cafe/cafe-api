/**
 * @module utils/common
 */
const config = require('config');
const _ = require('lodash');

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get global variable/environment variable/config variable
 * 1. When you are in local environment, you will get key-value from ./config/dev.json
 * 2. When you are in other environments, you will get key-value from Openshift
 * environment variable/config map
 * @param  {string} key - The key you want to get
 * @returns value you want to get by the key
 */
const getConfig = (key) => {
  if (process.env.NODE_ENV !== 'dev') {
    const result = process.env[key] ? process.env[key] : null;
    return result;
  }
  return config.get(key);
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description In order to reject the invalid request content type,
 * we define some content types(separate into comma) we allow in ./config/dev.json.
 * If http request is mapping one of the content types, we don't reject
 * If http request is not mapping one of the content types, we reject
 * @param  {string} contentType - contentType from http request
 * @returns Whether we need reject the http request
 */
const isValidContentType = (contentType) => {
  const allowRequestContentType = getConfig('ALLOW_REQUEST_CONTENT_TYPE');
  const allowContentTypeArr = allowRequestContentType.split(',');
  return _.some(allowContentTypeArr, acceptType => `${contentType}`.includes(acceptType));
};

module.exports = {
  getConfig,
  isValidContentType,
};
