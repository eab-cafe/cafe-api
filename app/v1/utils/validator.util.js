const _ = require('lodash');
const moment = require('moment');

const formUtil = require('./form.util');
const questionUtils = require('./question.util');

const {
  TEXT: tTEXT, NUMBER, DATE: tDATE, TIME,
  DATE_TIME, DATE_FORMAT_1, TIME_FORMAT_1, DATE_TIME_FORMAT_1,
  DEFAULT_ANS,
} = require('../constants/importDataType');

const {
  ARRAY, ARRAYOBJECT, OBJECT, TEXT: aTEXT, START_DATE, END_DATE,
  START_TIME, END_TIME,
} = require('../constants/answerType');

const { DATE } = require('../constants/questionType');

module.exports.IsValidRuleDataType = ({
  value, type,
  // format = null
}) => {
  switch (type) {
    case tTEXT:
      return typeof value === 'string';
    case NUMBER:
      return typeof value === 'number';
    case tDATE:
      return moment(value, DATE_FORMAT_1);
    case TIME:
      return moment(value, TIME_FORMAT_1);
    case DATE_TIME:
      return moment(value, DATE_TIME_FORMAT_1);
    default:
      // Unhandled Types
      return false;
  }
};

module.exports.IsValidItemDataType = ({
  questionStructureData, answerStructureData, importData,
}) => {
  const qValue = questionStructureData.value();
  const qOptionalProps = questionStructureData.optionalProps();
  const aType = answerStructureData.valueType();
  const isWritable = answerStructureData.isWritable();

  const { value: iValue, type: iType, format: iFormat } = importData;
  // Fallback
  if (!isWritable) {
    return 'Question Not Writable';
  }
  // Write by valueType
  switch (aType) {
    case ARRAY:
    case ARRAYOBJECT: {
      // importValue = [refId1, refId2...]
      if (questionStructureData.questionType() === DATE) {
        return false;
      }
      if (!Array.isArray(iValue)) {
        return 'Not Align with Question\'s valueType';
      }
      const lostMandatoryFieldList = _.filter(iValue, item => !_.has(item, 'referenceId'));
      if (!_.isEmpty(lostMandatoryFieldList)) {
        return 'Some value(s) missing the required field(s), referenceId';
      }
      return _.filter(iValue,
        ({ referenceId, value }) => {
          if (_.get(qOptionalProps, 'referenceId') === referenceId && value) {
            return false;
          }
          return !_.some(qValue, { referenceId });
        });
    }
    case aTEXT:
      return this.IsValidRuleDataType({ value: iValue, type: iType, format: iFormat });
    case OBJECT:
      // switch (questionType) {
      //   case TEXT_INPUT:
      //     return this.IsValidRuleDataType({ value: importValue, type: importType, format });
      //   default:
      // NOT SUPPORTED
      return 'QuestionType not supported';
      // }
    default:
      // NOT SUPPORTED
      return 'QuestionType not supported';
  }
};

module.exports.checkDataType = ({
  form,
  ruleDataList,
  itemDataList,
}) => {
  const ruleDataErrorList = _.filter(form.importDataRules, (rule) => {
    const data = _.find(ruleDataList, dataItem => dataItem.referenceId === rule.referenceId);
    return !this.IsValidRuleDataType({ value: data.value, type: rule.importType });
  });
  const itemDataErrorList = _.filter(itemDataList, (itemData) => {
    const targetQuestion = _.find(formUtil.getQuestionList(form),
      ({ referenceId }) => referenceId === itemData.referenceId);
    if (!targetQuestion) {
      return 'Question Not Found';
    }
    const wrappedQuestion = new questionUtils.QuestionAnswer({
      ...targetQuestion.answers,
      questionType: targetQuestion.questionType,
    });
    const wrappedPropsValue = new questionUtils.AnswerMapper({
      type: wrappedQuestion.type,
      questionType: targetQuestion.questionType,
    });
    // As ES5 not support Getter and Setter
    // use function instead as a temporary solution
    const errorList = this.IsValidItemDataType({
      questionStructureData: wrappedQuestion,
      answerStructureData: wrappedPropsValue,
      importData: itemData,
    });
    return !_.isEmpty(errorList);
  });
  return [...ruleDataErrorList, ...itemDataErrorList];
};

module.exports.checkRuleType = (dataSetRules,
  ruleList = [tTEXT, NUMBER, tDATE, TIME, DATE_TIME, DEFAULT_ANS]) => {
  const dataSetRuleErrorList = _.filter(dataSetRules,
    dataSetRuleItem => ruleList.indexOf(dataSetRuleItem.importType) === -1);
  return dataSetRuleErrorList;
};

module.exports.checkQuestionTypeCondition = ({ form, itemDataList }) => _.filter(itemDataList,
  (item) => {
    const questionList = formUtil.getQuestionList(form);
    const targetQuestion = _.find(questionList, { referenceId: item.referenceId });
    const wrappedQuestion = new questionUtils.QuestionAnswer({
      ...targetQuestion.answers,
      questionType: targetQuestion.questionType,
    });
    switch (wrappedQuestion.questionType()) {
      case DATE: {
        return !_.every([START_DATE, END_DATE, START_TIME, END_TIME], (ans) => {
          const curValue = _.get(item.value, ans, null);
          if (curValue !== null && curValue !== '') {
            return moment(curValue).isValid();
          }
          return true;
        });
      }
      default:
        return false;
    }
  });

module.exports.checkUniqueness = (listToCheck, theValue) => _.indexOf(listToCheck, theValue) === -1;

module.exports.checkReferenceIdList = (dataSet, form) => {
  // Get the form items which used the referenceId,
  // including question's refId && importDataRules
  const itemList = formUtil.getQuestionList(form);
  const ruleList = formUtil.getReferenceDataRule(form);
  // Map it to referenceId only for filtering
  const refIdsforItem = _.map(itemList, item => item.referenceId);
  const refIdsforRule = _.map(ruleList, question => question.referenceId);
  const refIdsList = [...refIdsforItem, ...refIdsforRule];
  // Only refIds in ruleList are MANDATORY, rest are OPTIONAL
  const missedList = _.filter(refIdsforRule,
    refId => _.findIndex(dataSet, dataItem => dataItem.referenceId === refId) === -1);
  // Return all items with valid refIds
  const filtedList = _.filter(dataSet,
    dataItem => _.indexOf(refIdsList, dataItem.referenceId) !== -1);
  return {
    missedList,
    filtedList,
  };
};

module.exports.checkImportDataRules = (dataResult, form) => {
  // Get the form items which used the referenceId,
  // including question's refId && importDataRules
  // const itemList = formUtil.getQuestionList(form);
  // const ruleList = formUtil.getReferenceDataRule(form);
  // // Map it to referenceId only for filtering
  // const refIdsforItem = _.map(itemList, item => (
  //   { questionDisplayId: item.displayId, referenceId: item.referenceId }
  // ));
  // const refIdsforRule = _.map(ruleList, question => question.referenceId);

  // // check miss ref data list
  // const missedList = _.filter(refIdsforRule, (refId) => {
  //   const item = _.find(refIdsforItem, o => o.referenceId === refId);
  //   if (item) {
  //     const displayId = _.get(item, 'questionDisplayId', '');
  //     return _.findIndex(_.get(dataResult, 'answers', []),
  //       dataItem => dataItem.questionDisplayId === displayId) === -1;
  //   }
  //   return true;
  // });
  const ruleList = formUtil.getReferenceDataRule(form);
  const refIdsforRule = _.map(ruleList, question => question.referenceId);
  const importData = _.get(dataResult, 'importData', []);
  const missedList = _.filter(refIdsforRule, refId => _.findIndex(importData,
    dataItem => dataItem.referenceId === refId) === -1);
  return missedList;
};
