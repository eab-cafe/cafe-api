module.exports = {
    extends: [
        "airbnb-base",
        "plugin:jest/recommended",
        "plugin:security/recommended"
    ],
    plugins: [
        "jest", 
        "security"
    ]
};