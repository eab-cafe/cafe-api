const mongoose = require('mongoose');

const Resource = new mongoose.Schema({
  languageMap: {
    type: Array,
    ref: 'Language',
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

const baseOptions = {
  discriminatorKey: 'type',
  collection: 'resource',
};

module.exports = mongoose.model('Resource', Resource, baseOptions);
