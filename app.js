const express = require('express');
const logger = require('morgan');
const log4js = require('log4js');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const indexRouter = require('./app/v1/routes/index.route');
const formRouter = require('./app/v1/routes/form.route');
const pageRouter = require('./app/v1/routes/page.route');
const sectionRouter = require('./app/v1/routes/section.route');
const questionRouter = require('./app/v1/routes/question.controller');
const responseRouter = require('./app/v1/routes/response.route');
const attachmentRouter = require('./app/v1/routes/attachment.route');
const resourceRouter = require('./app/v1/routes/resource.route');
const designThemeRouter = require('./app/v1/routes/designTheme.route');

const responseUtil = require('./app/v1/utils/response.util');
const log4jUtil = require('./app/v1/utils/log4j.util');
const commonUtil = require('./app/v1/utils/common.util');

// hello there!!! from today.
// Deleted in Group
// patrick chan
// ZLF
// XH
// No runners
// No runner 3
// no runner at all
// Delete in project
// ZLF from project
// Kelvin from project
// Ease Admin from project 
// James Ken Kevin
// Delete James Ken Kevin from group
// HJQ JT Felix
// edited on Gitlab
// without bernard
// without samuel
// sample code
// remove ssh

// const indexV2Router = require('./app/v2/routes/index.route');
// const testV2Router = require('./app/v2/routes/test.route');

const V2Router = require('./app/v2/routes/index.route');

// Init express framework
const app = express();

// Init Helmet security lib
// Included features by default:
// 1. dnsPrefetchControl(controls browser DNS prefetching),
// 2. frameguard(prevent clickjacking),
// 3. hidePoweredBy (remove the X-Powered-By header),
// 4. hsts (HTTP Strict Transport Security),
// 5. ieNoOpen (sets X-Download-Options for IE8+)
// 6. noSniff (keep clients from sniffing the MIME type)
// 7. xssFilter(adds some small XSS protections)
// ref to: https://helmetjs.github.io/docs/
app.use(helmet());
app.use(helmet.noCache());

// Init log4js
// level: ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF
// refer to: Configuration Object on https://log4js-node.github.io/log4js-node/api.html
const log4j = log4js.getLogger();
// log4js.configure({
//   appenders: {
//     everything: { type: 'file', filename: 'logs/records.log' },
//   },
//   categories: {
//     default: { appenders: ['everything'], level: 'debug' },
//   },
// });
log4j.level = 'debug';

// Use middleware
log4jUtil.log('info', 'Initializing middleware');
app.use(logger('[:date[iso]] :method :url :status - :response-time ms :res[content-length]'));
app.use(cors());

app.use((req, res, next) => {
  if (req.headers['content-type'] && !commonUtil.isValidContentType(req.headers['content-type'])) {
    responseUtil.generalBadRequestResponse(req, res, next, `Content type is not valid. Current content type: ${req.headers['content-type']}`);
  } else {
    next();
  }
});

// Set maximal request body size
// Express.js maximal request body default value is 100 kb
// The size can be set on global variable so that we can modify it conveniently.
// Refer to: https://www.npmjs.com/package/body-parser#limit
app.use(bodyParser.json({ limit: commonUtil.getConfig('REQUEST_BODY_MAX_SIZE') }));
app.use(bodyParser.urlencoded());

// Init router
log4jUtil.log('info', 'Initializing router');
// app.use('/v2', indexV2Router);
// app.use('/v2/test', testV2Router);
V2Router(app);
app.use('/', indexRouter);
app.use('/form', formRouter);
app.use('/page', pageRouter);
app.use('/section', sectionRouter);
app.use('/question', questionRouter);
app.use('/response', responseRouter);
app.use('/attachment', attachmentRouter);
app.use('/resource', resourceRouter);
app.use('/designTheme', designThemeRouter);

// Connect mongodb
log4jUtil.log('info', `mongodb url: ${commonUtil.getConfig('MONGODB_URL')}`);
log4jUtil.log('info', `mongodb db name: ${commonUtil.getConfig('MONGODB_DBNAME')}`);
const mongoUrl = commonUtil.getConfig('MONGODB_URL');
const mongoDBName = commonUtil.getConfig('MONGODB_DBNAME');
const mongoUserName = commonUtil.getConfig('MONGODB_USERNAME');
const mongoPassword = commonUtil.getConfig('MONGODB_PASSWORD');
mongoose.connect(`mongodb://${mongoUrl}/${mongoDBName}`, {
  useNewUrlParser: true,
  auth: {
    user: mongoUserName,
    password: mongoPassword,
  },
  autoReconnect: true,
  reconnectTries: commonUtil.getConfig('MONGODB_RECONNECT_TRIES'),
  reconnectInterval: commonUtil.getConfig('MONGODB_RECONNECT_INTERVAL'),
});
mongoose.connection.on('connected', () => {
  log4jUtil.log('info', 'Mongodb connect successfully');
  // FindAndModify is deprecated, we need disable it here globally
  // Ref to: https://stackoverflow.com/questions/52572852/deprecationwarning-collection-findandmodify-is-deprecated-use-findoneandupdate
  mongoose.set('useFindAndModify', false);
});
mongoose.connection.on('error', (err) => {
  log4jUtil.log('fatal', 'Connection failed with - ', err);
});
mongoose.connection.on('reconnected', (err) => {
  log4jUtil.log('info', 'Mongodb reconnected - ', err);
});
mongoose.connection.on('disconnected', (err) => {
  log4jUtil.log('info', 'Mongodb disconnected - ', err);
});
mongoose.connection.on('disconnecting', (err) => {
  log4jUtil.log('info', 'Mongodb disconnecting - ', err);
});
mongoose.connection.on('reconnected', (err) => {
  log4jUtil.log('info', 'Mongodb reconnected - ', err);
});

// catch 404 and forward to error handler
app.use((req, res) => {
  responseUtil.generalNotFoundResponse(req, res);
});

module.exports = app;
