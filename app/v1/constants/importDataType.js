/* TYPE */
const TEXT = 'TEXT';
const NUMBER = 'NUMBER';
const DATE = 'DATE';
const TIME = 'TIME';
const DATE_TIME = 'DATE_TIME';
const DEFAULT_ANS = 'DEFAULT_VALUE';

/* FORMAT */
const DATE_FORMAT_1 = 'DD/MM/YYYY'; // DATE
const TIME_FORMAT_1 = 'HH:mm'; // TIME
const DATE_TIME_FORMAT_1 = `${DATE_FORMAT_1} ${TIME_FORMAT_1}`; // DATE&TIME

const ON = 'ON';
const OFF = 'OFF';

const TEXT_INPUT = 'TEXT_INPUT';
const DROP_DOWN = 'DROP_DOWN';
module.exports = {
  TEXT,
  NUMBER,
  DATE,
  TIME,
  DATE_TIME,
  DEFAULT_ANS,
  DATE_FORMAT_1,
  TIME_FORMAT_1,
  DATE_TIME_FORMAT_1,
  ON,
  OFF,
  TEXT_INPUT,
  DROP_DOWN,
};
