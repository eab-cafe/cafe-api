/**
 * @module controller/response
 */
const _ = require('lodash');
const moment = require('moment');

const { ObjectId } = require('mongoose').Types;

const Response = require('../models/response');
const FormPublish = require('../models/formPublish');
const responseUtil = require('../utils/response.util');
const validateUtil = require('../utils/validator.util');
const formUtil = require('../utils/form.util');
const log4jUtil = require('../utils/log4j.util');
const questionUtils = require('../utils/question.util');
const useValidationUtils = require('../utils/useValidation.util');
const mqSender = require('../utils/mqSender.util');

const {
  TEXT_INPUT,
  SELECTOR,
  DROP_DOWN,
  TOGGLE,
  DATE,
  INPUT_DATE,
  INPUT_TIME,
  INPUT_DATE_N_TIME,
  TABLE,
} = require('../constants/questionType');
const {
  DATE_FORMAT_1, ON,
} = require('../constants/importDataType');

const {
  START_DATE,
  END_DATE,
  START_TIME, END_TIME,
} = require('../constants/answerType');

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @author Enhanced:: felix.chum <felix.chum@eabsystems.com>
 * @function
 * @description get Response Completeness, completed or not
 * @param {string} form - form data
 * @param {string} answers - response's answers data
 * @returns Boolean, form completed or not
 */
const getResponseCompleteness = ({
  form, answers, importData = [], visibilityMap = {},
}) => {
  const allQuestion = formUtil.getQuestionListFilterPageVisibility(form, importData);
  const allCheckQuestion = formUtil.getQuestionListFilterVisibility(
    allQuestion, answers, visibilityMap,
  );
  const allAnswers = answers;
  let status = true;
  if (allCheckQuestion && allCheckQuestion.length > 0) {
    const validationFunc = (question) => {
      const { isMandatory, validationRule } = question;
      return (isMandatory
          && _.findIndex(allAnswers,
            { questionDisplayId: question.displayId }) === -1)
           || useValidationUtils.useValidation({ validationRule, allAnswers });
    };
    const curStatus = _.some(allCheckQuestion, question => validationFunc(question));
    if (curStatus) {
      status = false;
    }
  }
  return status;
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description get response date DP format from mongodb
 * @param {string} responseId - response id which the response belongs to
 * @param {string} req.body.ownerID - owner id which the mas
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response data map to DP format info
 */
const getResponseDataToDPDtl = async (responseId) => {
  const resultInfo = {};
  const responseResult = await Response.findById(responseId).catch(error => ({ error }));
  // ? If Error
  if (_.has(responseResult, 'error')) {
    log4jUtil.log('warn', `Response not exist. responseId: ${responseId}`);
    resultInfo.status = 'error';
    resultInfo.message = `Response ${responseId} not exists.`;
  } else if (responseResult) {
    // ? Error Free
    const formPublish = await FormPublish.findById(responseResult.publishFormId)
      .catch(error => ({ error }));
    // ? If Error
    if (_.has(formPublish, 'error')) {
      log4jUtil.log('warn', `publishForm not exist. publishFormId: ${responseResult.publishFormId}`);
      resultInfo.status = 'error';
      resultInfo.message = `publishForm ${responseResult.publishFormId} not exists.`;
    } else if (formPublish) {
      // ? Error Free
      const result = {};
      const allQuestion = formUtil.getQuestionList(formPublish);
      _.forEach(responseResult.answers, (answer) => {
        const newQtn = {};
        const targetQuestion = _.find(allQuestion,
          ({ displayId }) => displayId === answer.questionDisplayId);
        newQtn.questionRefId = targetQuestion.referenceId;
        const wrappedQuestion = new questionUtils.QuestionAnswer({
          ...targetQuestion.answers,
          questionType: targetQuestion.questionType,
        });
        const newValue = {};
        newValue.type = answer.answer.valueType;

        if (answer.answer.type === INPUT_DATE || answer.answer.type === INPUT_TIME
          || answer.answer.type === INPUT_DATE_N_TIME) {
          const dateValue = {};
          _.forEach(answer.answer.value, (v) => {
            _.set(dateValue, v.type, v.value);
          });
          newValue.value = dateValue;
        } else
        if (answer.answer.isDisplayId && _.isEqual(answer.answer.valueType, 'array')) {
          const newValues = [];
          const newContents = [];
          if ((typeof answer.answer.value) === 'string') {
            const answerRefObj = _.find(wrappedQuestion.value(),
              { displayId: answer.answer.value });
            newValues.push(answerRefObj.referenceId);
            newContents.push(answerRefObj.text);
          } else {
            _.forEach(answer.answer.value, (v) => {
              const answerRefObj = _.find(wrappedQuestion.value(),
                { displayId: v });
              newValues.push(answerRefObj.referenceId);
              newContents.push(answerRefObj.text);
            });
          }
          if (answer.answer.optionalValue) {
            const refId = _.get(wrappedQuestion.optionalProps(), 'referenceId');
            // newValues.push(refId);
            newContents.push(answer.answer.optionalValue);
            newValues.push({
              [refId]: answer.answer.optionalValue,
            });
          }
          // TODO
          // ? newValue.content is not using
          if (_.isEmpty(newValues)) {
            newValue.value = '';
            newValue.content = '';
          } else if (newValues.length > 1) {
            newValue.value = newValues;
            newValue.content = newContents;
          } else {
            newValue.value = _.head(newValues);
            newValue.content = _.head(newContents);
          }
        } else {
          newValue.value = answer.answer.value;
        }
        newQtn.value = newValue;
        _.set(result, newQtn.questionRefId, newValue.value);
      });
      result.responseID = responseId;
      result.formID = responseResult.publishFormId;
      result.isCompleted = getResponseCompleteness({
        form: formPublish,
        answers: responseResult.answers,
        importData: responseResult.importData,
      });
      // ? Mapping the importedData be exportable
      // ! We are not validating the referenceId uniqueness
      _.forEach(responseResult.importData, (item) => {
        const { referenceId, value } = item;
        _.setWith(result, referenceId, value, typeof value);
      });
      resultInfo.status = 'success';
      resultInfo.result = result;
    } else {
      log4jUtil.log('warn', `publishForm not exist. publishFormId: ${responseResult.publishFormId}`);
      resultInfo.status = 'error';
      resultInfo.message = `publishForm ${responseResult.publishFormId} not exists.`;
    }
  } else {
    resultInfo.status = 'error';
    resultInfo.message = `Response ${responseId} not exists.`;
  }
  return resultInfo;
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description Get the DisplayId of all the questions in formPublish
 * @param {object} formPublish - from mongodb by formPublishId
 * @returns  All Question DisplayId array
 */
const getAllQtnDisplayId = (formPublish) => {
  const qtnDisplayIds = [];
  if (formPublish && formPublish.pages && formPublish.pages.length > 0) {
    formPublish.pages.forEach((page) => {
      if (page && page.sections && page.sections.length > 0) {
        page.sections.forEach((section) => {
          if (section && section.questions && section.questions.length > 0) {
            section.questions.forEach((question) => {
              if (question) {
                qtnDisplayIds.push(question.displayId);
              }
            });
          }
        });
      }
    });
  }
  return qtnDisplayIds;
};

/**
 * @author felix.chum <felix.chum@eabsystems.com>
 * @function
 * @description Get the DisplayId of all the SUB-questions in formPublish
 * @param {object} formPublish - from mongodb by formPublishId
 * @returns  All Question DisplayId array
 */
const getAllSubQuestionDisplayId = (formPublish) => {
  const pages = _.get(formPublish, 'pages', []);
  const sections = _.flatten(_.map(pages, page => page.sections));
  const questions = _.flatten(_.map(sections, section => section.questions));
  const questionsHasSubQuestion = _.filter(
    questions, ({ questionType }) => [TABLE].indexOf(questionType) > -1,
  );

  const subQuestions = _.flatten(_.map(questionsHasSubQuestion, (question) => {
    const wrappedQuestion = new questionUtils.QuestionAnswer({
      ...question.answers,
      questionType: question.questionType,
    });
    return wrappedQuestion.value();
  }));
  return _.map(subQuestions, ({ displayId }) => displayId);
};

// const getAllQuestion = (formPublish) => {
//   const allSection = _.flatten(_.map(_.get(formPublish, 'pages', []), page => page.sections));
//   const allQuestion = _.flatten(_.map(allSection, section => section.questions));
//   return allQuestion;
// };

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description Verify that the QuestionDisplayId is legal
 * @param {Array}  answers - express request answers
 * @param {object} formPublish - from mongodb by formPublishId
 * @returns Illegal DisplayId array
 */
const checkAnswersQuestionDisplayId = (answers, formPublish) => {
  const errDisplayId = [];
  // ? All Standard Questions displayId
  const questionDisplayIds = getAllQtnDisplayId(formPublish);
  // ? All Sub-Questions displayId
  const subQuestionDisplayIds = getAllSubQuestionDisplayId(formPublish);
  if (answers && answers.length > 0) {
    answers.forEach((answer) => {
      const isExistQuestion = questionDisplayIds.indexOf(answer.questionDisplayId) > -1;
      const isExistSubQuestion = _.some(subQuestionDisplayIds,
        subQuestionId => answer.questionDisplayId.includes(subQuestionId));
      if (!isExistQuestion && !isExistSubQuestion) {
        errDisplayId.push(answer.questionDisplayId);
      }
    });
  }
  return errDisplayId;
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @author felix.chum <felix.chum@eabsystems.com>
 * @function sendMessageToMQ
 * @description Verify that the QuestionDisplayId is legal
 * @param {Object} req - express request
 * @param {Object} res - express response
 * @param {Object} next - express next
 * @param {object} mqParameter - mqParamter
 * @param {String} mqParameter.channel - mqChannel name
 * @param {String} mqParameter.docType - mqDocType
 * @param {String} mqParameter.dataType - mqDataType || Value:: 'Y' or Anything else
 * @param {object} responseData - response object
 * @returns mqStatus
 */
const sendMessageToMQ = async ({
  mqParameter, responseData = {},
}) => {
  const { channel, docType } = mqParameter || {};
  const { _id: responseId } = responseData;
  if (!responseData || !responseId) {
    // eslint-disable-next-line no-throw-literal
    throw {
      code: 400,
      message: 'Response data is not valid',
    };
    // responseUtil.generalBadRequestResponse(req, res, next,
    // 'Response data is not valid');
  }
  if (mqParameter && !mqParameter.channel) {
    // eslint-disable-next-line no-throw-literal
    throw {
      code: 400,
      message: 'mqParameter passed without required parameter(s)',
    };
    // responseUtil.generalBadRequestResponse(req, res, next,
    // 'mqParameter passed without required parameter(s)');
  }
  // ? We always pass the formData with mqDataType && responseId
  const formData = {
    mqDataType: mqParameter.dataType || 'Y',
    responseID: responseId,
  };
  if (formData.mqDataType === 'Y') {
    const transformData = await getResponseDataToDPDtl(responseId);
    const { status: transformStatus, result: transformResult } = transformData;
    if (transformStatus !== 'success') {
      // eslint-disable-next-line no-throw-literal
      throw {
        code: 500,
        message: 'Unexpected Error(s) occurs in data transformation',
      };
      // responseUtil.generalInternalServerErrorResponse(req, res, next,
      // 'Unexpected Error(s) occurs in data transformation');
    }
    _.setWith(formData, 'data', transformResult, typeof transformResult);
  }
  const mqDocId = await mqSender(channel, docType, formData);
  // eslint-disable-next-line consistent-return
  return mqDocId;
};

/**
 * @deprecated use sendMessageToMQ instead
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function saveDoneSendMessageToMQ
 * @description transform the responseObj to object KVP structure
 * @param {Object} req - express request object
 * @param {Object} req.body - express request body
 * @param {String} req.body.mqChannel - mqChannel name
 * @param {String} req.body.mqDocType - mqDocType
 * @param {String} req.body.mqDataType - mqDataType || Value:: 'Y' or Anything else
 * @param {String} req.body.saveType - cafe saveType || Value:: 'submit' || 'next' || 'back'
 * @param {Array}  answers - express request answers
 * @param {object} formPublish - from mongodb by formPublishId
 * @returns Illegal DisplayId array
 */
const saveDoneSendMessageToMQ = async (req, responseObj) => {
  if (req.body.mqChannel && req.body.mqDocType && req.body.mqDataType && req.body.saveType === 'submit') {
    const sendMessage = {};
    sendMessage.mqDataType = req.body.mqDataType;
    if (sendMessage.mqDataType === 'Y') {
      const mappingDate = await getResponseDataToDPDtl(responseObj.id);
      if (mappingDate.status === 'success') {
        sendMessage.data = mappingDate.result;
      }
    } else {
      sendMessage.responseID = responseObj.id;
    }
    const mqStatus = await mqSender(req.body.mqChannel, req.body.mqDocType, sendMessage);
    return mqStatus;
  }
  return '';
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description Save Response information to mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.publishFormId - formPublish id which the response belongs to
 * @param {string} req.body.responseId - response id which the response belongs to
 * @param {Array} req.body.answers - response answers
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns new Response from mongodb
 */
module.exports.saveResponse = async (req, res, next) => {
  if (req.body.publishFormId) {
    let formPublish = null;
    try {
      formPublish = await FormPublish.findById(req.body.publishFormId).lean().exec();
    } catch (error) {
      formPublish = { error };
    }
    if (_.has(formPublish, 'error')) {
      responseUtil.generalBadRequestResponse(req, res, next, formPublish.error);
    } else {
      const errDisplayIds = checkAnswersQuestionDisplayId(req.body.answers, formPublish);
      if (errDisplayIds.length === 0) {
        const result = {};
        if (req.body.responseId) {
          const response = await Response.findOne(
            { _id: req.body.responseId }, null, { lean: true },
          );
          if (response) { // update old response data
            const newResponseData = {};
            newResponseData.importData = response.importData;
            newResponseData.answers = req.body.answers ? req.body.answers : [];
            newResponseData.createdAt = response.createdAt;
            newResponseData.updatedAt = moment();
            newResponseData.visibilityMap = {
              ...response.visibilityMap,
              ...req.body.visibilityMap,
            };
            newResponseData.isCompleted = getResponseCompleteness({
              form: formPublish,
              answers: req.body.answers,
              importData: response.importData,
              visibilityMap: newResponseData.visibilityMap,
            });
            const newResponse = await Response.findOneAndUpdate({
              _id: req.body.responseId,
            }, newResponseData, {
              new: true,
            });
            result.cafeResponse = newResponse;
            const docId = await saveDoneSendMessageToMQ(req, newResponse);
            if (docId && docId !== 'error') {
              result.MQ_docId = docId;
            }
            responseUtil.generalSuccessResponse(req, res, next, result);
          } else {
            responseUtil.generalBadRequestResponse(req, res, next, `Response ${req.body.responseId} not exists`);
          }
        } else { // add new response data
          const newResponseData = {};
          newResponseData.publishFormId = req.body.publishFormId;
          newResponseData.visibilityMap = req.body.visibilityMap;
          // newResponseData.isCompleted = false;
          newResponseData.isCompleted = getResponseCompleteness({
            form: formPublish,
            answers: req.body.answers,
            importData: [],
            visibilityMap: newResponseData.visibilityMap,
          });
          newResponseData.answers = req.body.answers ? req.body.answers : [];
          const newResponse = new Response(newResponseData);
          const saveNewResponse = await newResponse.save();
          result.cafeResponse = saveNewResponse;
          const docId = await saveDoneSendMessageToMQ(req, saveNewResponse);
          if (docId && docId !== 'error') {
            result.MQ_docId = docId;
          }
          responseUtil.generalSuccessResponse(req, res, next, result);
        }
      } else {
        responseUtil.generalBadRequestResponse(req, res, next, `Question ${_.join(errDisplayIds, ', ')} not exists`);
      }
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'publishFormId are required');
  }
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description Get response information from mongodb
 * @param {object} req - express request param
 * @param {object} req.params - express param body
 * @param {string} req.params.responseId - response id which the response belongs to
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response info
 */
module.exports.getResponse = async (req, res, next) => {
  const responseResult = await Response.findById(req.params.responseId).catch(error => ({ error }));
  if (_.has(responseResult, 'error')) {
    // responseUtil.generalBadRequestResponse(req, res, next, responseResult.error);
    log4jUtil.log('warn', `Response not exist. responseId: ${req.params.responseId}`);
    responseUtil.generalBadRequestResponse(req, res, next, `Response ${req.params.responseId} not exists.`);
  } else {
    const formPublish = await FormPublish.findById(responseResult.publishFormId)
      .catch(error => ({ error }));
    if (_.has(formPublish, 'error')) {
      responseUtil.generalBadRequestResponse(req, res, next, formPublish.error);
    } else if (formPublish) {
      const result = {};
      if (formPublish.importDataRuleStatus === ON) {
        const missDataList = validateUtil.checkImportDataRules(responseResult, formPublish);
        if (_.isEmpty(missDataList)) {
          result.status = 'Success';
          result.data = responseResult;
        } else {
          result.status = 'ImportMissing';
          result.data = missDataList;
        }

        responseUtil.generalSuccessResponse(req, res, next, result);
      } else {
        result.status = 'Success';
        result.data = responseResult;
        responseUtil.generalSuccessResponse(req, res, next, result);
      }
    }
  }
};

/**
 * @author Felix Chum <felix.chum@eabsystems.com>
 * @function saveImportData
 * @description embed external data to cafe's form responses
 * @param {object} req - express request param
 * @param {object} req.params - params of express request param
 * @param {object} publishFormId - the formId that published
 * @param {object} req.params.importData - the data passed for embed
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 */
module.exports.saveImportData = async (req, res, next) => {
  // Assuming that you will not have pass as an existing response
  const { body: { publishFormId, importData, mqParameter } } = req;
  // const { channel, docType } = mqParameter || {};
  // if (mqParameter && !mqParameter.channel) {
  //   responseUtil.generalBadRequestResponse(req, res, next,
  // 'mqParameter passed without required parameter(s)');
  //   return;
  // }
  // ! Error if publishForm not Valid
  if (!publishFormId) {
    responseUtil.generalBadRequestResponse(req, res, next, 'publishFormId is required');
    return;
  }
  try {
    const formPublish = await FormPublish.findById(req.body.publishFormId, null, { lean: true });
    const {
      missedList, filtedList,
    } = validateUtil.checkReferenceIdList(importData, formPublish);
      // ! Error if ReferenceId not Valid
    if (!_.isEmpty(missedList)) {
      responseUtil.generalBadRequestResponse(req, res, next, `Value(s) of ${missedList.join(', ')} are required`);
      return;
    }
    const ruleDataList = formUtil.getDataList(filtedList,
      formUtil.getReferenceDataRule(formPublish));
    const itemDataList = formUtil.getDataList(filtedList, formUtil.getQuestionList(formPublish));
    const misTypeList = validateUtil.checkDataType(
      { form: formPublish, ruleDataList, itemDataList },
    );
      // ! Error if Type not Valid
    if (!_.isEmpty(misTypeList)) {
      const errorRefId = _.map(misTypeList, ({ referenceId }) => referenceId).join(', ');
      const errorType = _.map(misTypeList, ({ importType }) => importType).join(', ');
      responseUtil.generalBadRequestResponse(req, res, next, `Value(s) of [${errorRefId}] misaligned with its type [${errorType}] as required`);
      return;
    }
    const misFormattedValueList = validateUtil.checkQuestionTypeCondition(
      { form: formPublish, itemDataList },
    );
    // ! Error if not matched with questionType required format
    if (!_.isEmpty(misFormattedValueList)) {
      const errorRefId = _.map(misFormattedValueList, ({ referenceId }) => referenceId).join(', ');
      responseUtil.generalBadRequestResponse(req, res, next, `Value(s) of [${errorRefId}] mismatched with its questionType required format of value`);
      return;
    }
    const newResponseData = {
      publishFormId,
      importData: ruleDataList,
    };
    if (!_.isEmpty(itemDataList)) {
      const mappedAnswer = _.map(itemDataList, (item) => {
        const targetQuestion = _.find(formUtil.getQuestionList(formPublish),
          ({ referenceId }) => referenceId === item.referenceId);
        const wrappedQuestion = new questionUtils.QuestionAnswer({
          ...targetQuestion.answers,
          questionType: targetQuestion.questionType,
        });
        const wrappedAnswer = new questionUtils.AnswerMapper({
          type: _.get(wrappedQuestion, '_type', null),
          questionType: targetQuestion.questionType,
        });

        const getAnswer = (type) => {
          const dictList = _.map(wrappedQuestion.value(),
            ({ displayId, referenceId }) => ({ referenceId, displayId }));
          const optionalValueList = _.filter(item.value,
            ({ referenceId }) => _.get(wrappedQuestion.optionalProps(), 'referenceId') === referenceId);
          const predefineValueList = _.filter(item.value,
            preValue => optionalValueList.indexOf(preValue) === -1);
          const mappedValue = _.map(predefineValueList,
            ({ referenceId }) => _.find(dictList,
              dict => dict.referenceId === referenceId).displayId);
          if (!_.isEmpty(optionalValueList)) {
            wrappedAnswer.setOptionalValue(_.first(optionalValueList).value);
          }
          if (!_.isEmpty(mappedValue)) {
            switch (type) {
              case 'single':
                wrappedAnswer.setValue(_.first(mappedValue));
                break;
              default:
                wrappedAnswer.setValue(mappedValue);
                break;
            }
          }
        };

        const getDateAnswer = () => {
          if (typeof item.value === 'object') {
            const mappedValue = _.map([START_DATE, END_DATE, START_TIME, END_TIME], itemTemp => ({
              type: itemTemp,
              value: _.get(item.value, itemTemp, null),
            }));
            wrappedAnswer.setValue(mappedValue);
          } else {
            const dictList = wrappedQuestion.value();
            const mappedValue = _.map(item.value, (dataItem) => {
              const dictItem = _.find(dictList, { referenceId: dataItem.referenceId });
              return {
                type: dictItem.type,
                value: [START_DATE, END_DATE].indexOf(dictItem.type) > -1 ? moment(dataItem.value, DATE_FORMAT_1).format('X') : dataItem.value,
              };
            });
            wrappedAnswer.setValue(mappedValue);
          }
        };

        switch (wrappedQuestion.questionType()) {
          case SELECTOR:
            getAnswer(wrappedQuestion.type === 'SINGLE_ANSWER' ? 'single' : null);
            break;
          case DROP_DOWN:
          case TOGGLE:
            getAnswer();
            break;
          case DATE:
            getDateAnswer();
            break;
          case TEXT_INPUT:
            wrappedAnswer.setValue(item.value);
            break;
          default:
            break;
        }
        return {
          questionDisplayId: targetQuestion.displayId,
          answer: wrappedAnswer.ToMap(),
        };
      });
      newResponseData.answers = mappedAnswer;
      newResponseData.isCompleted = getResponseCompleteness({
        form: formPublish,
        answers: newResponseData.answers,
        importData: newResponseData.importData,
        visibilityMap: {},
      });
    }
    // ? 1. We save the Response to create the responseId first
    const newResponse = new Response(newResponseData);
    const saveNewResponse = await newResponse.save();
    const saveNewResponseResult = await Response.findById(saveNewResponse.id, null, { lean: true });
    // ? 2. We send the ResponseData though MessageQueue
    if (mqParameter) {
      const docId = await sendMessageToMQ({ mqParameter, responseData: saveNewResponseResult });
      if (docId === 'error') {
        responseUtil.generalBadRequestResponse(req, res, next, 'Unexpected Error(s) occurs in MessageQueue Communication');
        return;
      }
      saveNewResponse.MQ_docId = docId;
    }
    responseUtil.generalSuccessResponse(req, res, next, saveNewResponseResult);
  } catch (error) {
    // ! Unknown error
    log4jUtil.log('fatal', error);
    switch (typeof error) {
      case 'object': {
        const { code, message } = error;
        switch (code) {
          case 400:
            responseUtil.generalBadRequestResponse(req, res, next, message);
            break;
          case 500:
            responseUtil.generalInternalServerErrorResponse(req, res, next, message);
            break;
          default:
            responseUtil.generalInternalServerErrorResponse(req, res, next, error);
            break;
        }
      }
        break;
      default:
        responseUtil.generalInternalServerErrorResponse(req, res, next, error);
        break;
    }
  }
};

module.exports.updateImportData = async (req, res, next) => {
  const { body: { importData, responseId } } = req;
  try {
    const responseItem = await Response.findById(responseId, null, { lean: true });
    const { publishFormId } = responseItem;
    const formPublish = await FormPublish.findById(publishFormId, null, { lean: true });
    const {
      missedList, filtedList,
    } = validateUtil.checkReferenceIdList(importData, formPublish);
    if (!_.isEmpty(missedList)) {
      responseUtil.generalBadRequestResponse(req, res, next, `Value(s) of ${missedList.join(', ')} are required`);
      return;
    }
    const misTypeList = validateUtil.checkDataType(importData, formPublish);
    if (!_.isEmpty(misTypeList)) {
      const errorRefId = _.map(misTypeList, ({ referenceId }) => referenceId).join(', ');
      const errorType = _.map(misTypeList, ({ importType }) => importType).join(', ');
      responseUtil.generalBadRequestResponse(req, res, next, `Value(s) of [${errorRefId}] misaligned with its type [${errorType}] as required`);
      return;
    }
    const newResponseData = {
      ..._.omit(responseItem, ['_id']),
      updatedAt: moment(),
      importData: filtedList,
    };
    const newResponse = await Response.findOneAndUpdate((responseId),
      newResponseData, { new: true });
    responseUtil.generalSuccessResponse(req, res, next, newResponse);
  } catch (error) {
    log4jUtil.log('error', error);
    responseUtil.generalInternalServerErrorResponse(req, res, next, error);
  }
};


/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description Get response List from mongodb
 * @param {object} req - express request param
 * @param {object} req.params - express param body
 * @param {string} req.params.publishFormId -  - formPublish id which the response belongs to
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response info
 */
module.exports.getResponseList = async (req, res, next) => {
  if (req.params.publishFormId) {
    const formPublish = await FormPublish.findById(req.params.publishFormId)
      .catch(error => ({ error }));
    if (_.has(formPublish, 'error')) {
      responseUtil.generalBadRequestResponse(req, res, next, formPublish.error);
    } else if (formPublish) {
      const responses = await Response.find({ publishFormId: req.params.publishFormId });
      responseUtil.generalSuccessResponse(req, res, next, responses);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'publishFormId are required');
  }
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description get response date mas format from mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.publishFormId - formPublish id which the response belongs to
 * @param {string} req.body.responseId - response id which the response belongs to
 * @param {string} req.body.ownerID - owner id which the mas
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response data map to mas format info
 */
module.exports.getResponseDataToMas = async (req, res, next) => {
  const {
    body: {
    // ownerID, publishFormId,
      responseId,
    },
  } = req;

  const responseResult = await Response.findById(responseId).catch(error => ({ error }));
  if (_.has(responseResult, 'error')) {
    log4jUtil.log('warn', `Response not exist. responseId: ${responseId}`);
    responseUtil.generalBadRequestResponse(req, res, next, `Response ${responseId} not exists.`);
  } else if (responseResult) {
    const formPublish = await FormPublish.findById(responseResult.publishFormId)
      .catch(error => ({ error }));
    if (_.has(responseResult, 'error')) {
      log4jUtil.log('warn', `publishForm not exist. publishFormId: ${responseResult.publishFormId}`);
      responseUtil.generalBadRequestResponse(req, res, next, `publishForm ${responseResult.publishFormId} not exists.`);
    } else {
      const result = {};
      const allQuestion = formUtil.getQuestionList(formPublish);
      const newAnswers = [];
      _.forEach(responseResult.answers, (answer) => {
        const newQtn = {};
        const targetQuestion = _.find(allQuestion,
          ({ displayId }) => displayId === answer.questionDisplayId);
        newQtn.questionRefId = targetQuestion.referenceId;
        const wrappedQuestion = new questionUtils.QuestionAnswer({
          ...targetQuestion.answers,
          questionType: targetQuestion.questionType,
        });
        const newValue = {};
        newValue.type = answer.answer.valueType;
        if (answer.answer.isDisplayId && _.isEqual(answer.answer.valueType, 'array')) {
          const newValues = [];
          const newContents = [];
          if ((typeof answer.answer.value) === 'string') {
            const answerRefObj = _.find(wrappedQuestion.value(),
              { displayId: answer.answer.value });
            newValues.push(answerRefObj.referenceId);
            newContents.push(answerRefObj.text);
          } else {
            _.forEach(answer.answer.value, (v) => {
              const answerRefObj = _.find(wrappedQuestion.value(),
                { displayId: v });
              newValues.push(answerRefObj.referenceId);
              newContents.push(answerRefObj.text);
            });
          }
          if (answer.answer.optionalValue) {
            const refId = _.get(wrappedQuestion.optionalProps(), 'referenceId');
            newValues.push(refId);
            newContents.push(answer.answer.optionalValue);
          }
          newValue.value = newValues;
          newValue.content = newContents;
        } else {
          newValue.content = answer.answer.value;
          newValue.value = wrappedQuestion.value().referenceId;
        }
        newQtn.value = newValue;
        newAnswers.push(newQtn);
      });
      result.answers = newAnswers;
      responseUtil.generalSuccessResponse(req, res, next, result);
    }
  }
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description get response status from mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.publishFormId - formPublish id which the response belongs to
 * @param {string} req.body.responseId - response id which the response belongs to
 * @param {string} req.body.ownerID - owner id which the mas
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response status map to mas
 */
module.exports.getResponseStatus = async (req, res, next) => {
  const {
    body: {
    // ownerID, publishFormId,
      responseId,
    },
  } = req;
  const responseResult = await Response.findById(responseId).catch(error => ({ error }));
  if (_.has(responseResult, 'error')) {
    log4jUtil.log('warn', `Response not exist. responseId: ${responseId}`);
    responseUtil.generalBadRequestResponse(req, res, next, `Response ${responseId} not exists.`);
  } else if (responseResult) {
    const formPublish = await FormPublish.findById(responseResult.publishFormId)
      .catch(error => ({ error }));
    if (_.has(responseResult, 'error')) {
      log4jUtil.log('warn', `publishForm not exist. publishFormId: ${responseResult.publishFormId}`);
      responseUtil.generalBadRequestResponse(req, res, next, `publishForm ${responseResult.publishFormId} not exists.`);
    } else {
      const result = {};
      result.status = getResponseCompleteness({
        form: formPublish,
        answers: responseResult.answers,
        importData: responseResult.importData,
        visibilityMap: responseResult.visibilityMap,
      });
      result.createAt = responseResult.createdAt;
      result.updateAt = responseResult.updatedAt;
      responseUtil.generalSuccessResponse(req, res, next, result);
    }
  }
};

module.exports.deleteResponse = async (req, res, next) => {
  try {
    if (!req.params.responseId) {
      responseUtil.generalBadRequestResponse(req, res, next, 'responseId is required');
      return;
    }
    if (!ObjectId.isValid(req.params.responseId)) {
      responseUtil.generalBadRequestResponse(req, res, next, 'Not a valid responseId');
      return;
    }
    const result = await Response.findOne({ _id: req.params.responseId }).catch(err => err);
    if (!result) {
      responseUtil.generalBadRequestResponse(req, res, next, 'No response belongs to the provided responseId');
      return;
    }
    await Response.findOneAndDelete({ _id: req.params.responseId });
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: req.params.responseId,
    });
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

/**
 * @author xue.hua <xue.hua@eabsystems.com>
 * @function
 * @description get response date DP format from mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.publishFormId - formPublish id which the response belongs to
 * @param {string} req.body.responseId - response id which the response belongs to
 * @param {string} req.body.ownerID - owner id which the mas
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Response data map to DP format info
 */
module.exports.getResponseDataToDP = async (req, res, next) => {
  const {
    body: {
    // ownerID, publishFormId,
      responseId,
    },
  } = req;
  const resultInfo = await getResponseDataToDPDtl(responseId);
  if (resultInfo.status === 'success') {
    responseUtil.generalSuccessResponse(req, res, next, resultInfo.result);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, resultInfo.message);
  }
};
