

const request = require('request-promise');
const axios = require('axios');
const commonUtil = require('./common.util');
const log4jUtil = require('./log4j.util');

const minIoURL = commonUtil.getConfig('MinIO_URL');
const minIoFileServiceName = commonUtil.getConfig('MinIO_FILE_SERVICE_NAME');
const minIoBucketName = commonUtil.getConfig('MinIO_BUCKET_NAME');

const uploadFileThroughFileApi = async (
  fileId,
  fileName,
  fileStream,
) => {
  const options = {
    method: 'PUT',
    url: `${minIoURL}/file-api/${minIoFileServiceName}/${minIoBucketName}/${fileId}`,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    formData: {
      file: {
        value: fileStream,
        options: {
          filename: fileName,
          contentType: null,
        },
      },
    },
  };
  log4jUtil.log('info', 'Call file api to upload');
  const result = await request(options).catch(error => ({ error: error.cause.message }));
  log4jUtil.log('result:', result);
  if (result.error) {
    return result;
  }
  log4jUtil.log('result:', result);
  return {};
};
const getFileUrlThroughFileApi = async (
  fileId,
) => {
  const url = await axios.request({
    baseURL: minIoURL,
    url: `${minIoURL}/file-api/${minIoFileServiceName}/${minIoBucketName}/${fileId}`,
    method: 'GET',
  }).then(result => result).catch(error => ({ error }));
  return url.data;
};
const deleteFileUrlThroughFileApi = async (
  fileId,
) => {
  const url = await axios.request({
    baseURL: minIoURL,
    url: `${minIoURL}/file-api/${minIoFileServiceName}/${minIoBucketName}/${fileId}`,
    method: 'DELETE',
  }).then(result => result).catch(error => ({ error }));
  return url.data;
};
module.exports = {
  uploadFileThroughFileApi,
  getFileUrlThroughFileApi,
  deleteFileUrlThroughFileApi,
};
