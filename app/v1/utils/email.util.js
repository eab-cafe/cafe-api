const nodemailer = require('nodemailer');
const commonUtil = require('./common.util');

const MAIL_HOST = commonUtil.getConfig('MAIL_HOST');
const MAIL_PORT = commonUtil.getConfig('MAIL_PORT');
const MAIL_AUTHOR = commonUtil.getConfig('MAIL_AUTHOR');
const MAIL_FROM = commonUtil.getConfig('MAIL_FROM');
const MAIL_TO = commonUtil.getConfig('MAIL_TO');
// const MAIL_CC = commonUtil.getConfig('MAIL_CC');
const MAIL_BCC = commonUtil.getConfig('MAIL_BCC');
const ENV = commonUtil.getConfig('ENV');

const smtpTransport = nodemailer.createTransport({
  host: MAIL_HOST,
  port: MAIL_PORT,
  secure: false, // disable SSL
  requireTLS: true, // Force TLS
  tls: {
    rejectUnauthorized: false,
  },
  // auth: {
  //     user: '***@163.com',
  //     pass: '***'//注：此处为授权码，并非邮箱密码
  // }
});

const sendMail = options => new Promise((resolve) => {
  const optionsWithHeader = {
    from: `"${MAIL_AUTHOR}" <${MAIL_FROM}>`,
    to: MAIL_TO,
    bcc: MAIL_BCC,
    // cc: MAIL_CC,
    subject: `${ENV} - [FATAL] ERROR occurred in Cafe-API `,
    ...options,
  };
  /*
  var _options = {
        from        : '“huang.junfeng” <huang.junfeng@eabsystems.com>',
        to          : 'kevin.liang@eabsystems.com',
        cc         : 'xue.hua@eabsystems.com,huang.junfeng@eabsystems.com' , //抄送
        // bcc      : ''    //密送
        subject        : '一封来自Node Mailer的邮件',
        text          : '一封来自Node Mailer的邮件',
        html           : '<h1>你好，这是一封来自NodeMailer的邮件！</h1><p><img src="cid:00000001"/></p>',
        // attachments :
        //             [
        //                 {
        //                     filename: 'img1.png',            // 改成你的附件名
        //                     path: 'public/images/img1.png',  // 改成你的附件路径
        //                     cid : '00000001'                 // cid可被邮件使用
        //                 },
        //                 {
        //                     filename: 'img2.png',            // 改成你的附件名
        //                     path: 'public/images/img2.png',  // 改成你的附件路径
        //                     cid : '00000002'                 // cid可被邮件使用
        //                 },
        //             ]
    };
    */
  smtpTransport.sendMail(optionsWithHeader, (err, msg) => {
    if (err) {
      resolve({
        success: false,
        message: `Mail fail ${err}`,
      });
      return;
    }
    resolve({
      success: true,
      message: `Mail sent ${msg}`,
    });
  });
});

module.exports = {
  sendMail,
};
