const faker = require('faker');

const commonUtil = require('../../app/v1/utils/common.util');

describe('Common Util', () => {
  it('[function]getConfig [cause]ENV != dev and process.env[key] not exists', () => {
    process.env.NODE_ENV = 'sit';
    expect(commonUtil.getConfig('MAAM_JWKS_URL')).toBe(null);
  });

  it('[function]getConfig [cause]ENV != dev and process.env[key] exists', () => {
    const fakeValue = faker.internet.url();
    process.env.NODE_ENV = 'sit';
    process.env.MAAM_JWKS_URL = fakeValue;
    expect(commonUtil.getConfig('MAAM_JWKS_URL')).toBe(fakeValue);
  });
});
