const mongoose = require('mongoose');

const QuestionSchema = new mongoose.Schema({
  displayId: {
    type: String,
  },
  title: {
    type: String,
    // required: true,
  },
  questionType: {
    type: String,
  },
  answers: {
    type: Object,
    default: {},
  },
  isDisable: {
    type: Boolean,
    default: false,
  },
  isMandatory: {
    type: Boolean,
    default: false,
  },
  isReadonly: {
    type: Boolean,
    default: false,
  },
  designSetting: {
    type: Object,
    default: {},
  },
  gridOrder: {
    type: Number,
  },
  gridSpan: {
    type: Number,
  },
  visibilityRule: {
    type: Array,
    default: [],
  },
  validationRule: {
    type: Array,
    default: [],
  },
  relationRule: {
    type: Array,
    default: [],
  },
  referenceId: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  trackList: {
    type: Array,
    default: [],
  },
  rootId: {
    type: String,
    default: null,
  },
}, { minimize: false });

module.exports = mongoose.model('Question', QuestionSchema);
