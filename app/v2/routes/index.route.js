const express = require('express');

const router = express.Router();

const testRouter = require('./test.route');

const formsRouter = require('./forms.route');

const responseUtil = require('../utils/response.util');

router.get('/', (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, 'Hello World v2');
});

function V2Router(app, basePath = '/v2') {
  app.use(`${basePath}`, router);
  app.use(`${basePath}/test`, testRouter);

  app.use(`${basePath}/forms`, formsRouter);
}

module.exports = V2Router;
// module.exports = router;
