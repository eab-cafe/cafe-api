const mongoose = require('mongoose');

const PageModel = new mongoose.Schema({
  displayId: {
    type: String,
  },
  title: {
    type: String,
    // required: true,
  },
  description: {
    type: String,
  },
  sections: {
    type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
    ref: 'Section',
    default: [],
  },
  designSetting: {
    type: Object,
    default: {},
  },
  visibilityRule: {
    type: Array,
    default: [],
  },
  referenceId: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
}, { minimize: false });

module.exports = mongoose.model('Page', PageModel);
