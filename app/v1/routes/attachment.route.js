const express = require('express');
const multer = require('multer');

const upload = multer({ dest: 'tmp/' });

const router = express.Router();

const attachmentController = require('../controller/attachment.controller');

router.post('/', upload.single('image'), (req, res, next) => {
  attachmentController.saveAttachment(req, res, next);
  // attachmentController.getAttachment(req, res, next);
});

router.get('/', (req, res, next) => {
  attachmentController.getAttachment(req, res, next);
});


router.post('/:imageId/upload', upload.single('image'), (req, res, next) => {
  attachmentController.minIoUploadFile(req, res, next);
});

router.get('/:imageId/upload', (req, res, next) => {
  attachmentController.getMinIoUploadFile(req, res, next);
});
router.delete('/:formId/upload/:imageId', (req, res, next) => {
  attachmentController.deleteMinIoUploadFile(req, res, next);
});
module.exports = router;
