const express = require('express');

const router = express.Router();

// const responseUtil = require('../utils/response.util');

const formController = require('../controller/forms.controller');

router.get('/', async (req, res, next) => {
  formController.getForms(req, res, next);
});

router.get('/:formId', async (req, res, next) => {
  formController.getForm(req, res, next);
});

module.exports = router;
