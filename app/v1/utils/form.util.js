
const _ = require('lodash');
const moment = require('moment');
const {
  PAGE,
  SECTION_CARD,
  QUESTION_CARD,
  INPUT_DATE,
  MUITIPLE_ANSWER,
  INPUT_DATE_N_TIME,
  INPUT_TIME,
} = require('../constants/questionType');
const {
  ARRAY, ARRAYOBJECT, OBJECT,
} = require('../constants/answerType');
// ! REMAPPER
const {
  IS_VISIBLE,
  ENABLE,
  IS_EQUAL,
  IS_NOT_EQUAL,
  LESS_THAN,
  GREATER_THAN,
  LESS_THAN_OR_EQUAL,
  GREATER_THAN_OR_EQUAL,
  MC_CHOSEN_VALUES_CONTAINS,
  MC_NOT_CHOSEN_VALUES_CONTAINS,
  MC_HAS_OPTIONS,
  OTH_LESS_THAN,
  OTH_GREATER_THAN,
  OTH_LESS_THAN_OR_EQUAL,
  OTH_GREATER_THAN_OR_EQUAL,
  CHOOSE_NUM_GREATER,
  CHOOSE_NUM_LESS,
  MC_ONLY_VALUE_CHOSEN,
  HAS_ANSWER,
  HAS_NO_ANSWER,
} = require('../constants/advanceSetting');
const { START_DATE } = require('../constants/answerType');
const { DATE, ON } = require('../constants/importDataType');

const utils = this;
const questionUtils = require('./question.util');

// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-ADMIN
// ! LOCATION: utils/index.js
// Move to co-exist project in future if needed
function getPageList(form, filterPage = null) {
  let pageList = form.pages;
  if (filterPage) {
    pageList = pageList.filter(page => page.displayId !== filterPage.displayId);
  }
  return pageList;
}

// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-ADMIN
// ! LOCATION: utils/formUtils.js
// Move to co-exist project in future if needed
const getFlattenFormItems = (form = null, maxDepth = null) => {
  const pageList = utils.getPageList(form);
  const debug = _.flattenDeep(
    _.map(pageList, (page) => {
      if (maxDepth === PAGE) {
        return page;
      }
      const sections = _.map(page.sections, (section) => {
        if (maxDepth === SECTION_CARD) {
          return section;
        }
        const questions = _.map(section.questions, (question) => {
          if (maxDepth === QUESTION_CARD) {
            return question;
          }
          const wrappedQuestion = new questionUtils.QuestionAnswer({
            ...question.answers,
            questionType: question.questionType,
          });
          let result = null;
          if (!_.isEmpty(wrappedQuestion.value())) {
            switch (wrappedQuestion.valueType()) {
              case OBJECT:
                // console.warn(
                //   `${question.questionType}:: OBJECT:: referenceId:: `,
                //   wrappedQuestion.value
                // );
                result = [question, wrappedQuestion.value()];
                break;
              case ARRAYOBJECT: {
                // console.warn(
                //   `${question.questionType}:: ARRAYOBJECT:: referenceId:: `,
                //   _.map(wrappedQuestion.value, referenceId => referenceId)
                // );
                const answers = _.map(wrappedQuestion.value(), (wrappedAnswer) => {
                  const { text, ...otherProps } = wrappedAnswer;
                  return {
                    title: text,
                    ...otherProps,
                  };
                });
                result = [question, ...answers];
                break;
              }
              case ARRAY: {
                // console.warn(
                //   `${question.questionType}:: ARRAY:: referenceId:: `,
                //   _.map(wrappedQuestion.value, referenceId => referenceId)
                // );
                // return [question, ...wrappedQuestion.value];
                const answers = _.map(wrappedQuestion.value(), (wrappedAnswer) => {
                  const { text, ...otherProps } = wrappedAnswer;
                  return {
                    title: text,
                    ...otherProps,
                  };
                });
                result = [question, ...answers];
                break;
              }
              default:
                console.error(
                  `${question.questionType}:: using unhandled valueType ${
                    wrappedQuestion.valueType()
                  }`,
                );
                result = question;
            }
          } else {
            result = question;
          }
          if (
            wrappedQuestion.optionalProps()
              && wrappedQuestion.optionalProps().hasOptions
          ) {
            result.push(wrappedQuestion.optionalProps());
          }
          return result;
        });
        return [section, ...questions];
      });
      return [page, ...sections];
    }),
  );
    // console.warn("debug", debug);
  return debug;
};

// ! CAVEAT
// ! DATE: 16/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-ADMIN
// ! LOCATION: utils/formUtils.js
// Move to co-exist project in future if needed
const getReferenceDataRule = (form) => {
  const rules = form.importDataRules;
  return rules;
};

// ! CAVEAT
// ! DATE: 18/9/2019
// ! TYPE: DUPLICATE
// ! PROJECT: CAFE-ADMIN
// ! LOCATION: utils/index.js
const getQuestionList = (form, filterQuestion = null) => {
  const pageList = form.pages;
  const allSection = _.flatten(pageList.map(page => page.sections));
  let allQuestion = _.flatten(allSection.map(section => section.questions));
  if (filterQuestion) {
    allQuestion = allQuestion.filter(
      quest => quest.displayId !== filterQuestion.displayId,
    );
  }
  return allQuestion;
};

const getDataList = (dataList, itemList) => _.filter(dataList,
  ({ referenceId }) => _.some(itemList, { referenceId }));

const getQuestionListFilterPageVisibility = (form, importData, filterQuestion = null) => {
  const pageVisibilityState = _.get(
    form,
    'setting.pageVisibilityProp.pageVisibilityState',
    '',
  );
  const importDataRuleStatus = _.get(form, 'importDataRuleStatus', '');
  const importDataRules = _.get(form, 'importDataRules', []);
  let pageList = form.pages;
  if (pageVisibilityState === ENABLE && importDataRuleStatus === ON) {
    const checkVisibility = (originalValue) => {
      const { id, condition, value } = originalValue;
      const rule = _.find(importDataRules, dataRule => dataRule.displayId === id);
      if (!rule) {
        // rule not exists
        return false;
      }
      const { referenceId, importType } = rule;
      const curImportData = _.find(
        importData,
        data => data.referenceId === referenceId,
      );
      if (!curImportData) {
        // data not exists
        return false;
      }
      // original
      const conditionCheck = (valueInForm, visibilityValue = value) => {
        // const isDate = utils.dateUtils.isDate(valueInForm);
        switch (condition) {
          case IS_EQUAL:
            if (importType === DATE) {
              return moment(valueInForm).isSame(visibilityValue);
            }
            return valueInForm === visibilityValue;
          case IS_NOT_EQUAL:
            if (importType === DATE) {
              return !moment(valueInForm).isSame(visibilityValue);
            }
            return valueInForm !== visibilityValue;
          case LESS_THAN:
            // if (isDate) {
            //   return utils.dateUtils.isBefore(valueInForm, visibilityValue);
            // }
            if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
              return Number(valueInForm) < Number(visibilityValue);
            }
            return valueInForm < visibilityValue;
          case GREATER_THAN:
            if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
              return Number(valueInForm) > Number(visibilityValue);
            }
            return valueInForm > visibilityValue;
          case LESS_THAN_OR_EQUAL:
            if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
              return Number(valueInForm) <= Number(visibilityValue);
            }
            return valueInForm <= visibilityValue;
          case GREATER_THAN_OR_EQUAL:
            if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
              return Number(valueInForm) >= Number(visibilityValue);
            }
            return valueInForm >= visibilityValue;
          default:
            return false;
        }
      };

      switch (
        importType // DATE, NUMBER, ON, TEXT
      ) {
        case DATE:
          return conditionCheck(_.get(importData, 'value.startDate', null));
        default:
          return conditionCheck(_.get(importData, 'value', null));
      }
    };
    pageList = _.filter(_.get(form, 'pages', []), (page) => {
      const visibilityRule = _.get(page, 'visibilityRule', []);
      if (!_.isEmpty(visibilityRule)) {
        return visibilityRule.some(orLogic => orLogic.every(andLogic => checkVisibility(andLogic)));
      }
      return true;
    });
  }

  const allSection = _.flatten(pageList.map(page => page.sections));
  let allQuestion = _.flatten(allSection.map(section => section.questions));
  if (filterQuestion) {
    allQuestion = allQuestion.filter(
      quest => quest.displayId !== filterQuestion.displayId,
    );
  }
  return allQuestion;
};

const getQuestionListFilterVisibility = (allQuestion, allAnswers, visibilityMap = {}) => {
  const checkVisibility = (originalValue) => {
    const { id, condition, value } = originalValue;

    const valueMap = _.find(allAnswers, { questionDisplayId: id });
    // original
    const conditionCheck = (
      valueInForm,
      optionalValue,
      visibilityValue = value,
    ) => {
      // valueInForm means the form's value. What user input in the form
      // visibilityValue means the visibility value itself. Since there is
      // some case need to change the value to compare. it will be value by default

      const mappingType = _.get(valueMap, 'answer.type', '');
      const isDate = INPUT_DATE === mappingType && moment(valueInForm).isValid();
      const optValue = !_.isNaN(optionalValue) ? Number(optionalValue) : 0;
      const optCnt = optionalValue && optionalValue !== null && optionalValue !== '' ? 1 : 0;
      switch (condition) {
        case IS_VISIBLE:
          return _.get(visibilityMap, id, false);
        case IS_EQUAL:
        case MC_CHOSEN_VALUES_CONTAINS:
          if (isDate) {
            return moment(valueInForm).isSame(visibilityValue);
          }
          return (
            valueInForm === visibilityValue
            || (visibilityValue === MC_HAS_OPTIONS && optCnt > 0)
          );
        case IS_NOT_EQUAL:
        case MC_NOT_CHOSEN_VALUES_CONTAINS:
          if (isDate) {
            return !moment(valueInForm).isSame(visibilityValue);
          }
          return (
            (visibilityValue !== MC_HAS_OPTIONS && valueInForm !== visibilityValue)
            || (visibilityValue === MC_HAS_OPTIONS && optCnt === 0)
          );
        case LESS_THAN:
          if (isDate) {
            return moment(valueInForm).isBefore(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) < Number(visibilityValue);
          }
          return valueInForm < visibilityValue;
        case GREATER_THAN:
          if (isDate) {
            return moment(valueInForm).isAfter(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) > Number(visibilityValue);
          }
          return valueInForm > visibilityValue;
        case LESS_THAN_OR_EQUAL:
          if (isDate) {
            return moment(valueInForm).isSameOrBefore(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) <= Number(visibilityValue);
          }
          return valueInForm <= visibilityValue;
        case GREATER_THAN_OR_EQUAL:
          if (isDate) {
            return moment(valueInForm).isSameOrAfter(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) >= Number(visibilityValue);
          }
          return valueInForm >= visibilityValue;
        case OTH_LESS_THAN:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) < Number(visibilityValue);
          }
          return optionalValue && optValue < visibilityValue;
        case OTH_GREATER_THAN:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) > Number(visibilityValue);
          }
          return optionalValue && optValue > visibilityValue;
        case OTH_LESS_THAN_OR_EQUAL:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) <= Number(visibilityValue);
          }
          return optionalValue && optValue <= visibilityValue;
        case OTH_GREATER_THAN_OR_EQUAL:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) >= Number(visibilityValue);
          }
          return optionalValue && optValue >= visibilityValue;
        case CHOOSE_NUM_GREATER:
          return valueInForm.length + optCnt > Number(visibilityValue);
        case CHOOSE_NUM_LESS:
          return valueInForm.length + optCnt < Number(visibilityValue);
        case MC_ONLY_VALUE_CHOSEN:
          return (
            valueInForm.length + optCnt === 1
            && ((visibilityValue !== MC_HAS_OPTIONS
              && _.head(valueInForm) === visibilityValue)
              || (visibilityValue === MC_HAS_OPTIONS && optCnt > 0))
          );
        case HAS_ANSWER:
          return !!valueInForm || !_.isEmpty(optionalValue);
        case HAS_NO_ANSWER:
          return !valueInForm;
        default:
          return false;
      }
    };
    const mappingType = _.get(valueMap, 'answer.type', '');
    const mappingValue = _.get(valueMap, 'answer.value', '');
    const optionalValue = _.get(valueMap, 'answer.optionalValue', '');
    // Different compare function
    // console.log("id:: mappingType:: ", mappingType);
    // console.log("id:: mappingValue:: ", mappingValue);
    switch (mappingType) {
      case MUITIPLE_ANSWER:
        // if (typeof mappingValue !== "string" && !_.isEmpty(mappingValue)) {
        //   return mappingValue.some(val => conditionCheck(val, optionalValue));
        // } else {
        //   return conditionCheck(mappingValue, optionalValue);
        // }
        if (
          originalValue.condition === CHOOSE_NUM_GREATER
          || originalValue.condition === CHOOSE_NUM_LESS
          || originalValue.condition === MC_ONLY_VALUE_CHOSEN
        ) {
          return conditionCheck(mappingValue, optionalValue);
        } if (
          originalValue.condition.indexOf('OTH_') === -1
          && mappingValue.length > 0
          && typeof mappingValue !== 'string'
        ) {
          if (originalValue.condition === MC_NOT_CHOSEN_VALUES_CONTAINS) {
            return mappingValue.every(val => conditionCheck(val, optionalValue));
          }
          return mappingValue.some(val => conditionCheck(val, optionalValue));
        }
        return conditionCheck(mappingValue, optionalValue);

      case INPUT_DATE_N_TIME:
      case INPUT_TIME:
        // ? We bypass the checking
        // return timeCheck();
        // return timeCheck() && dateCheck();
        return true;
      case INPUT_DATE: {
        const propsValueStartDate = _.find(mappingValue, { type: START_DATE })
          .value;
        const momentPropsValueStartDate = propsValueStartDate
          ? new Date(propsValueStartDate).toISOString() : null;
        return conditionCheck(momentPropsValueStartDate);
      }
      default:
        return conditionCheck(mappingValue);
    }
  };
  const checkQtnVisibility = (question) => {
    const visibilityRule = _.get(question, 'visibilityRule', []);
    if (!_.isEmpty(visibilityRule)) {
      return visibilityRule.some(orLogic => orLogic.every(andLogic => checkVisibility(andLogic)));
    }
    return true;
  };
  const returnQuestion = allQuestion.filter(question => checkQtnVisibility(question));
  return returnQuestion;
};

module.exports.getDataList = getDataList;
module.exports.getQuestionList = getQuestionList;
module.exports.getPageList = getPageList;
module.exports.getFlattenFormItems = getFlattenFormItems;
module.exports.getReferenceDataRule = getReferenceDataRule;
module.exports.getQuestionListFilterPageVisibility = getQuestionListFilterPageVisibility;
module.exports.getQuestionListFilterVisibility = getQuestionListFilterVisibility;
