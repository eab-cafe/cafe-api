const express = require('express');


const router = express.Router();

const designThemeController = require('../controller/designTheme.controller');

router.get('/', (req, res, next) => {
  designThemeController.getDesignThemes(req, res, next);
});
module.exports = router;
