const express = require('express');

const router = express.Router();

const questionController = require('../controller/question.controller');

router.post('/', (req, res, next) => {
  questionController.saveQuestion(req, res, next);
});

router.get('/:questionId', (req, res, next) => {
  questionController.getQuestion(req, res, next);
});

router.put('/:questionId', (req, res, next) => {
  questionController.updateQuestion(req, res, next);
});

router.delete('/:questionId', (req, res, next) => {
  questionController.deleteQuestion(req, res, next);
});

module.exports = router;
