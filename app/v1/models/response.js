const mongoose = require('mongoose');

const ResponseSchema = new mongoose.Schema({
  publishFormId: {
    type: String,
    required: true,
  },
  importData: [
    {
      referenceId: {
        type: String,
        required: true,
      },
      value: {
        type: Object,
        required: true,
      },
    },
  ],
  answers: [
    {
      questionDisplayId: {
        type: String,
        required: true,
      },
      answer: {
        type: Object,
        default: {},
      },
    },
  ],
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  isCompleted: {
    type: Boolean,
    default: false,
    required: true,
  },
  visibilityMap: {
    type: Object,
    default: {},
  },
}, { minimize: false });

module.exports = mongoose.model('Response', ResponseSchema);
