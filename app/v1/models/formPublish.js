const mongoose = require('mongoose');

const FormPublishModel = new mongoose.Schema({
  ownerFormId: {
    type: String,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  icon: {
    type: String,
  },
  importDataRuleStatus: {
    type: String,
  },
  importDataRules: [{
    displayId: {
      type: String,
      required: true,
    },
    referenceId: {
      type: String,
      required: true,
    },
    importType: {
      type: String,
      required: true,
    },
    format: {
      type: String,
    },
  }],
  pages: [
    {
      displayId: {
        type: String,
      },
      title: {
        type: Object,
      // required: true,
      },
      description: {
        type: Object,
      },
      sections: [
        {
          displayId: {
            type: String,
          },
          title: {
            type: Object,
          // required: true,
          },
          description: {
            type: Object,
          },
          questions: [
            {
              displayId: {
                type: String,
              },
              title: {
                type: Object,
              // required: true,
              },
              questionType: {
                type: String,
              },
              answers: {
                type: Object,
                default: {},
              },
              logicRules: {
                type: [String],
                default: [],
              },
              isDisable: {
                type: Boolean,
                default: false,
              },
              isMandatory: {
                type: Boolean,
                default: false,
              },
              designSetting: {
                type: Object,
                default: {},
              },
              gridOrder: {
                type: Number,
              },
              gridSpan: {
                type: Number,
              },
              visibilityRule: {
                type: Array,
                default: [],
              },
              validationRule: {
                type: Array,
                default: [],
              },
              relationRule: {
                type: Array,
                default: [],
              },
              trackList: {
                type: Array,
                default: [],
              },
              rootId: {
                type: String,
                default: null,
              },
              referenceId: {
                type: String,
                default: '',
              },
              createdAt: {
                type: Date,
                default: Date.now,
                required: true,
              },
              updatedAt: {
                type: Date,
                default: Date.now,
                required: true,
              },
            },
          ],
          designSetting: {
            type: Object,
            default: {},
          },
          columnCount: {
            type: Number,
          },
          visibilityRule: {
            type: Array,
            default: [],
          },
          referenceId: {
            type: String,
            default: '',
          },
          createdAt: {
            type: Date,
            default: Date.now,
            required: true,
          },
          updatedAt: {
            type: Date,
            default: Date.now,
            required: true,
          },
        },
      ],
      designSetting: {
        type: Object,
        default: {},
      },
      visibilityRule: {
        type: Array,
        default: [],
      },
      referenceId: {
        type: String,
        default: '',
      },
      createdAt: {
        type: Date,
        default: Date.now,
        required: true,
      },
      updatedAt: {
        type: Date,
        default: Date.now,
        required: true,
      },
    },
  ],
  designSetting: {
    type: Object,
    default: {},
  },
  languageSetting: {
    isActivted: {
      type: Boolean,
    },
    list: [{
      code: {
        type: String,
        required: true,
      },
      label: {
        type: String,
      },
    }],
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  setting: {
    type: Object,
    default: {},
  },
}, { minimize: false });

module.exports = mongoose.model('FormPublish', FormPublishModel);
