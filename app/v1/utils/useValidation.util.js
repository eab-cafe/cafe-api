const _ = require('lodash');
const moment = require('moment');
const {
  MUITIPLE_ANSWER,
  INPUT_DATE,
  INPUT_TIME,
  INPUT_DATE_N_TIME,
} = require('../constants/questionType');

const {
  IS_EQUAL,
  IS_NOT_EQUAL,
  LESS_THAN,
  GREATER_THAN,
  LESS_THAN_OR_EQUAL,
  GREATER_THAN_OR_EQUAL,
  OTH_LESS_THAN,
  OTH_GREATER_THAN,
  OTH_LESS_THAN_OR_EQUAL,
  OTH_GREATER_THAN_OR_EQUAL,
  HAS_ANSWER,
  HAS_NO_ANSWER,
  AND,
  OR,
  CHOOSE_NUM_LESS,
  CHOOSE_NUM_GREATER,
  MC_CHOSEN_VALUES_CONTAINS,
  MC_NOT_CHOSEN_VALUES_CONTAINS,
  MC_ONLY_VALUE_CHOSEN,
  MC_HAS_OPTIONS,
} = require('../constants/advanceSetting');

const useValidation = (item) => {
  const { validationRule, allAnswers } = item;

  const checkValidationRule = (rule) => {
    const { qtnDisplayId, condition, value } = rule;
    const curMappingValue = _.find(allAnswers, { questionDisplayId: qtnDisplayId });
    // console.log('xh curMappingValue=', curMappingValue);
    const valueMap = _.get(curMappingValue, 'answer', {});
    const conditionCheck = (
      valueInForm,
      optionalValue,
      visibilityValue = value,
    ) => {
      // console.log('xh condition=', condition);
      // console.log('xh valueInForm=', valueInForm);
      // console.log('xh optionalValue=', optionalValue);
      // console.log('xh visibilityValue=', visibilityValue);
      const optValue = optionalValue && !_.isNaN(optionalValue) ? Number(optionalValue) : 0;
      const optCnt = optionalValue && optionalValue !== null && optionalValue !== '' ? 1 : 0;
      const mappingType = _.get(valueMap, `${qtnDisplayId}.type`, '');
      const isDate = INPUT_DATE === mappingType && moment(valueInForm).isValid();
      switch (condition) {
        case IS_EQUAL:
        case MC_CHOSEN_VALUES_CONTAINS:
          if (isDate) {
            return moment(valueInForm).isSame(visibilityValue);
          }
          return (
            valueInForm === visibilityValue
            || (visibilityValue === MC_HAS_OPTIONS && optCnt > 0)
          );
        case IS_NOT_EQUAL:
        case MC_NOT_CHOSEN_VALUES_CONTAINS:
          if (isDate) {
            return !moment(valueInForm).isSame(visibilityValue);
          }
          return (
            (visibilityValue !== MC_HAS_OPTIONS
              && valueInForm !== visibilityValue)
            || (visibilityValue === MC_HAS_OPTIONS && optCnt === 0)
          );
        case LESS_THAN:
          if (isDate) {
            return moment(valueInForm).isBefore(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) < Number(visibilityValue);
          }
          return valueInForm < visibilityValue;
        case GREATER_THAN:
          if (isDate) {
            return moment(valueInForm).isAfter(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) > Number(visibilityValue);
          }
          return valueInForm > visibilityValue;
        case LESS_THAN_OR_EQUAL:
          if (isDate) {
            return moment(valueInForm).isSameOrBefore(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) <= Number(visibilityValue);
          }
          return valueInForm <= visibilityValue;
        case GREATER_THAN_OR_EQUAL:
          if (isDate) {
            return moment(valueInForm).isSameOrAfter(visibilityValue);
          }
          if (!_.isNaN(valueInForm) && !_.isNaN(visibilityValue)) {
            return Number(valueInForm) >= Number(visibilityValue);
          }
          return valueInForm >= visibilityValue;
        case OTH_LESS_THAN:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) < Number(visibilityValue);
          }
          return optionalValue && optValue < visibilityValue;
        case OTH_GREATER_THAN:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) > Number(visibilityValue);
          }
          return optionalValue && optValue > visibilityValue;
        case OTH_LESS_THAN_OR_EQUAL:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) <= Number(visibilityValue);
          }
          return optionalValue && optValue <= visibilityValue;
        case OTH_GREATER_THAN_OR_EQUAL:
          if (optionalValue && !_.isNaN(optValue) && !_.isNaN(visibilityValue)) {
            return Number(optValue) >= Number(visibilityValue);
          }
          return optionalValue && optValue >= visibilityValue;
        case CHOOSE_NUM_GREATER:
          return valueInForm.length + optCnt > Number(visibilityValue);
        case CHOOSE_NUM_LESS:
          return valueInForm.length + optCnt < Number(visibilityValue);
        case MC_ONLY_VALUE_CHOSEN:
          return (
            valueInForm.length + optCnt === 1
            && ((visibilityValue !== MC_HAS_OPTIONS
              && _.head(valueInForm) === visibilityValue)
              || (visibilityValue === MC_HAS_OPTIONS && optCnt > 0))
          );
        case HAS_ANSWER:
          return !!valueInForm;
        case HAS_NO_ANSWER:
          return !valueInForm;
        default:
          return false;
      }
    };

    const dateCheck = () => false;
    const timeCheck = () => false;

    const mappingType = _.get(valueMap, 'type', '');
    const mappingValue = _.get(valueMap, 'value', '');
    const optionalValue = _.get(valueMap, 'optionalValue', '');

    // console.log('xh valueMap=', valueMap);
    // console.log('xh rule.mappingType=', mappingType);
    // console.log('xh mappingValue=', mappingValue);
    // Different compare function
    switch (mappingType) {
      case MUITIPLE_ANSWER:
        // if (rule.condition.indexOf('OTH_') === -1 && mappingValue.length > 0
        // && typeof mappingValue !== 'string') {
        //   return mappingValue.some(val => conditionCheck(val, optionalValue));
        // }
        // return conditionCheck(mappingValue, optionalValue);
        if (
          rule.condition === CHOOSE_NUM_GREATER
          || rule.condition === CHOOSE_NUM_LESS
          || rule.condition === MC_ONLY_VALUE_CHOSEN
        ) {
          return conditionCheck(mappingValue, optionalValue);
        } if (
          rule.condition.indexOf('OTH_') === -1
          && mappingValue.length > 0
          && typeof mappingValue !== 'string'
        ) {
          if (rule.condition === MC_NOT_CHOSEN_VALUES_CONTAINS) {
            return mappingValue.every(val => conditionCheck(val, optionalValue));
          }
          return mappingValue.some(val => conditionCheck(val, optionalValue));
        }
        return conditionCheck(mappingValue, optionalValue);

      case INPUT_DATE_N_TIME:
        return timeCheck() && dateCheck();
      case INPUT_DATE:
        return dateCheck();
      case INPUT_TIME:
        return timeCheck();
      default:
        return conditionCheck(mappingValue, optionalValue);
    }
  };
  const checkValidationGroup = (group) => {
    const { operator, rules } = group;
    if (rules && rules.length > 0) {
      if (operator === AND) {
        return rules.every(rule => checkValidationRule(rule));
      } if (operator === OR) {
        return rules.some(rule => checkValidationRule(rule));
      }
    }
    return false;
  };
  let result = false;
  // console.log("xh validationRule=", validationRule);
  if (validationRule && validationRule.length > 0) {
    //  Check the validationRule
    validationRule.forEach((ruleMsg) => {
      let ruleFail = false;
      const {
        canGoNextPage, operator, groups,
      } = ruleMsg;
      if (groups && groups.length > 0) {
        if (operator === AND) {
          ruleFail = groups.every(group => checkValidationGroup(group));
        } else if (operator === OR) {
          ruleFail = groups.some(group => checkValidationGroup(group));
        }
      }
      if (ruleFail && !canGoNextPage) {
        result = true;
      }
    });
  }
  return result;
};

module.exports = {
  useValidation,
};
