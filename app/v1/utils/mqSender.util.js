const uuid = require('uuid/v1');
const _ = require('lodash');
const MQHelper = require('./rabbitmq.util');
const log4jUtil = require('./log4j.util');

const sender = async (channelName, docType = null, mqData = {}) => {
  let result = '';
  const rabbit = new MQHelper({
    queue: channelName,
    option: MQHelper.getQueueOption(),
    role: MQHelper.ROLE.PUBLISHER,
  });
  await new Promise((resolve) => {
    const formData = mqData;
    if (docType) {
      formData.docType = docType;
    }
    log4jUtil.log('info', '[MQ] | Sender | Send Message | ', channelName, JSON.stringify(formData));
    rabbit.sendMessage({
      message: formData,
      onCallback: (_message) => {
        // console.log('MQ | SendQueue | callback1 ', _message.content.toString());
        log4jUtil.log('info', '[MQ] | Sender | onCallback | ', _message.content.toString());
        if (JSON.parse(_message.content.toString()).success) {
          result = _.get(JSON.parse(_message.content.toString()), 'result._id', '');
        } else {
          result = 'error';
        }
        resolve(_message);
      },
      queueOption: { correlationId: uuid() },
      consumeOption: { noAck: true },
    });
  });
  rabbit.closeConnection();
  return result;
  // process.exit(0);
};
module.exports = sender;
