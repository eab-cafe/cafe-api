# Stage 1
FROM mhart/alpine-node
EXPOSE 3000:3000

WORKDIR /opt/app-root/src
COPY . ./
RUN yarn
CMD ["npm", "run", "prod"]