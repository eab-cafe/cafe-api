const mongoose = require('mongoose');

const MinIoMappingSchema = new mongoose.Schema({
  fileServiceName: {
    type: String,
    default: 'minio',
  },
  bucketName: {
    type: String,
  },
  mappingLevel: {
    type: String,
  },
  mappingFormId:
  {
    type: String,
  },
  mappingId: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
  createdBy: {
    type: String,
    required: true,
    default: 'admin',
  },
  updatedDate: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedBy: {
    type: String,
    required: true,
    default: 'admin',
  },
}, { minimize: false });

module.exports = mongoose.model('MinIoMapping', MinIoMappingSchema);
