/* eslint-disable max-len */
/* eslint-disable consistent-return */
/* eslint-disable no-await-in-loop */
/**
 * @module controller/resource
 */
const _ = require('lodash');
const aigle = require('aigle');
const moment = require('moment');

const { ObjectId } = require('mongoose').Types;

const responseUtil = require('../utils/response.util');
// const log4jUtil = require('../utils/log4j.util');

/* Add different type of resources here */
const ResourceList = require('../models/resourceList');
const ResourceListItem = require('../models/resourceListItem');
const ResourceCascade = require('../models/resourceCascade');

const Language = require('../models/language');

/* Lodash to async */
aigle.mixin(_);

/* Static / Constant */
// const RESOURCE = 'RESOURCE';
const LIST = 'LIST';
const CASCADE = 'CASCADE';

// ? always return the new one
const findOneAndUpdateOptions = {
  new: true,
};

const modelTypeList = [
  {
    type: LIST,
    model: ResourceList,
  },
  {
    type: CASCADE,
    model: ResourceCascade,
  },
];

/* Methods */
const addChildItem = async ({
  model: ModelWrapper,
  records,
  hasDate = false,
}) => aigle.map(records, async (record) => {
  const dateMeta = hasDate ? {} : {
    createdAt: moment(),
    updatedAt: moment(),
  };
  const newRecord = new ModelWrapper({
    ..._.omit(record, ['_id']),
    ...dateMeta,
  });
  const saveResult = await newRecord.save();
  const getResult = await ModelWrapper.findById(saveResult.id).lean().exec();
  return getResult;
});

const updateChildItem = async ({
  model: ModelWrapper,
  records,
  date = {},
}) => {
  const resultList = [];
  for (let recordIndex = 0; recordIndex < records.length; recordIndex += 1) {
    const record = records[recordIndex];
    const isNewRecord = !_.has(record, '_id');
    if (isNewRecord) {
      const result = await addChildItem({ model: ModelWrapper, records: [record] });
      resultList.push(...result);
    } else {
      const { createdAt, _id, ...otherParams } = record;
      const dateMeta = {
        createdAt,
        updatedAt: moment(),
        ...date,
      };
      const updateResult = await ModelWrapper.findOneAndUpdate({ _id }, { ...otherParams, ...dateMeta }, findOneAndUpdateOptions).lean().exec();
      resultList.push(updateResult);
    }
  }
  return resultList;
  // const newRecordsList = _.filter(records, record => !_.has(record, '_id'));
  // const updateRecordsList = _.filter(records, record => _.has(record, '_id'));
  // const newRecordsSaveResult = await addChildItem({ model: ModelWrapper, records: newRecordsList });
  // const updateRecordsSaveResult = await aigle.map(updateRecordsList, async (record) => {
  //   const { createdAt, _id, ...otherParams } = record;
  //   const dateMeta = {
  //     createdAt,
  //     updatedAt: moment(),
  //     ...date,
  //   };
  //   const updateResult = await ModelWrapper.findOneAndUpdate({ _id }, { ...otherParams, ...dateMeta }, findOneAndUpdateOptions).lean().exec();
  //   return updateResult;
  // });
  // return [...updateRecordsSaveResult, ...newRecordsSaveResult];
};

const removeChildItem = async ({
  records,
  model: ModelWrapper,
}) => aigle.forEach(records, async ({ _id }) => {
  await ModelWrapper.findOneAndDelete({ _id }).exec();
});

module.exports.saveResource = async (req, res, next) => {
  const { data, type } = req.body;
  if (!data) {
    responseUtil.generalBadRequestResponse(req, res, next, 'data is required');
    return;
  }
  const { languageMap } = data;
  try {
    let newResource = null;
    const newLanguageMapResults = await addChildItem({ model: Language, records: languageMap, debug: 'lang' });
    switch (type) {
      case LIST: {
        const {
          list, key, title, referenceId,
        } = data;
        if (!key && !referenceId) {
          responseUtil.generalBadRequestResponse(req, res, next, 'Resource-List required key');
          return;
        }
        const newListItemIdResults = await addChildItem({ model: ResourceListItem, records: list, hasDate: true });
        newResource = new ResourceList({
          key,
          title,
          referenceId,
          languageMap: newLanguageMapResults,
          list: newListItemIdResults,
          createdAt: moment(),
          updatedAt: moment(),
        });
        break;
      }
      case CASCADE: {
        const {
          list, key, title, referenceId, mapping = [],
        } = data;
        if (!key && !referenceId) {
          responseUtil.generalBadRequestResponse(req, res, next, 'Resource-List required key');
          return;
        }
        const newListResults = await addChildItem({ model: ResourceList, records: list, hasDate: true });
        newResource = new ResourceCascade({
          key,
          title,
          referenceId,
          languageMap: newLanguageMapResults,
          list: newListResults,
          mapping,
          createdAt: moment(),
          updatedAt: moment(),
        });
        break;
      }
      /* Add your case here */
      default: {
        responseUtil.generalBadRequestResponse(req, res, next, 'Invalid type provided');
        return;
      }
    }
    const { id: saveId } = await newResource.save();
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: saveId,
    });
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

module.exports.getResource = async (req, res, next) => {
  const { resourceId, resourceType } = req.params;
  if (!resourceId) {
    responseUtil.generalBadRequestResponse(req, res, next, 'resourceId is required');
    return;
  }
  if (!ObjectId.isValid(resourceId)) {
    responseUtil.generalBadRequestResponse(req, res, next, 'Not a valid resourceId');
    return;
  }
  try {
    // ? If no resourceType provided, it will not filter out anything
    const pendingJobsList = _.filter(modelTypeList, ({ type }) => type !== resourceType);
    let jobsResultList = [];
    await aigle.forEach(pendingJobsList, async ({ model: ModelWrapper, type }) => {
      const hasFoundResult = _.some(jobsResultList, ({ result }) => !_.isEmpty(result));
      if (hasFoundResult) {
        // ? return false === break
        return false;
      }
      const searchResult = await ModelWrapper.findById(resourceId).lean().exec();
      if (searchResult) {
        jobsResultList.push({
          ...searchResult,
          type,
        });
      }
    });
    // if (getResult.error) {
    //   log4jUtil.log('warn', `ResourceCategory not exist. resourceId: ${resourceId}`);
    // }
    jobsResultList = _.filter(jobsResultList, result => !_.isEmpty(result));
    if (_.isEmpty(jobsResultList)) {
      responseUtil.generalBadRequestResponse(req, res, next, `Resource ${resourceId} not exists in every possible resource Types`);
      return;
    }
    const flattenJobResultList = _.flatten(jobsResultList);
    // ? It should only return 1 result.
    const firstResult = _.first(flattenJobResultList);
    responseUtil.generalSuccessResponse(req, res, next, firstResult);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

module.exports.getResources = async (req, res, next) => {
  try {
    const { type: reqType } = req.params;
    const filterModelTypeList = _.filter(modelTypeList, ({ type: modelType }) => (reqType ? modelType === reqType : true));
    const allResourcesResults = await aigle.map(filterModelTypeList,
      async ({ model: ModelWrapper, type }) => {
        const result = await ModelWrapper.find().lean().exec();
        let mapResultByType = _.map(result, item => ({
          ...item,
          type,
        }));
        // ? filter
        mapResultByType = _.filter(mapResultByType, item => !item.isHidden);
        return mapResultByType;
        // return _.map(result, item => ({
        //   ...item,
        //   type,
        // }));
      });
    // ? We need flatten to one level only
    const flatResourcesResults = _.flattenDepth(allResourcesResults, 2);
    responseUtil.generalSuccessResponse(req, res, next, flatResourcesResults);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

module.exports.deleteResource = async (req, res, next) => {
  const { resourceId } = req.params;
  if (!resourceId) {
    responseUtil.generalBadRequestResponse(req, res, next, 'resourceId is required');
    return;
  }
  if (!ObjectId.isValid(resourceId)) {
    responseUtil.generalBadRequestResponse(req, res, next, 'Not a valid resourceId');
    return;
  }
  try {
    const getModelWrapper = async () => _.find(modelTypeList, async (item) => {
      const { model: ModelWrapper } = item;
      const result = await ModelWrapper.findOne({ _id: resourceId }).exec();
      return !!result;
    });
    const { type, model: ModelWrapper } = await getModelWrapper();
    const dataResult = await ModelWrapper.findOne({ _id: resourceId }).lean().exec();
    if (!dataResult) {
      responseUtil.generalBadRequestResponse(req, res, next, 'Not a valid resourceId');
      return;
    }
    const { languageMap: langaugeMapList } = dataResult;
    switch (type) {
      case LIST: {
        const { list } = dataResult;
        await removeChildItem({ records: list, model: ResourceListItem });
        break;
      }
      default: {
        responseUtil.generalBadRequestResponse(req, res, next, 'No Valid resource Type found');
        return;
      }
    }
    await removeChildItem({ records: langaugeMapList, model: Language });
    await ModelWrapper.findOneAndDelete({ _id: resourceId }).exec();
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: req.params.resourceId,
    });
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

const updateTypeListResource = async (resourceId, data) => {
  const {
    languageMap, list, key, referenceId, title, isHidden,
  } = data;
  // ? 1. We update the LanguageMap
  const saveLanguageResults = await updateChildItem({ model: Language, records: languageMap, debug: 'lang' });
  // ? 2. We update the data
  // If there are no key && referenceId, which we cannot locate what file
  // ! [Deprecated] key
  if (!key && !referenceId) {
    throw new Error(JSON.stringify({
      type: 'badRequest',
      message: 'Resource-List required key || referenceId',
    }));
  }
  // ? 3. Find the Resource
  const resourceData = await ResourceList.findOne({ _id: resourceId }).lean().exec();
  if (!resourceData) {
    throw new Error(JSON.stringify({
      type: 'badRequest',
      message: 'Cannot find a record using the resourceId provided',
    }));
  }
  // ? 4. Remove the languageMap if newData removed
  const { languageMap: oldLanguageMap, list: oldList } = resourceData;
  const newLanguageMapIdList = _.map(_.filter(languageMap, item => _.has(item, '_id')), '_id');
  await removeChildItem({
    model: Language,
    records: _.filter(oldLanguageMap, item => (_.has(item, '_id') ? newLanguageMapIdList.indexOf(_.get(item, '_id')) === -1 : true)),
  });
  // ? 5. Retreive the ListItem
  const saveListItemResults = await updateChildItem({ model: ResourceListItem, records: list });
  // ? 6. Remove the ListItem if removed
  const newListIdList = _.map(_.filter(list, item => _.has(item, '_id')), '_id');
  await removeChildItem({
    model: ResourceListItem,
    records: _.filter(oldList, item => (_.has(item, '_id') ? newListIdList.indexOf(_.get(item, '_id')) === -1 : true)),
  });
  const saveResult = await ResourceList.findOneAndUpdate({ _id: resourceId }, {
    ..._.omit(resourceData, ['_id']),
    key,
    title,
    referenceId,
    isHidden,
    list: saveListItemResults,
    languageMap: saveLanguageResults,
    updatedAt: moment(),
  }, findOneAndUpdateOptions).lean().exec();
  return saveResult;
};

module.exports.updateResource = async (req, res, next) => {
  const { resourceId } = req.params;
  const { data, type } = req.body;
  if (!resourceId) {
    responseUtil.generalBadRequestResponse(req, res, next, 'Neither data is empty nor not valid');
    return;
  }
  try {
    let saveResult = {};
    // const { languageMap } = data;
    // const saveLanguageResults = await updateChildItem({ model: Language, records: languageMap, debug: 'lang' });
    switch (type) {
      case LIST: {
        saveResult = await updateTypeListResource(resourceId, data);
        // const {
        //   list, key, referenceId, title,
        // } = data;
        // if (!key && !referenceId) {
        //   responseUtil.generalBadRequestResponse(req, res, next, 'Resource-List required key || referenceId');
        //   return;
        // }
        // const resourceData = await ResourceList.findOne({ _id: resourceId }).lean().exec();
        // if (!resourceData) {
        //   responseUtil.generalBadRequestResponse(req, res, next, 'Cannot find a record using the resourceId provided');
        //   return;
        // }
        // // ? Remove the LanguageMap if removed
        // const { languageMap: oldLanguageMap, list: oldList } = resourceData;
        // const newLanguageMapIdList = _.map(_.filter(languageMap, item => _.has(item, '_id')), '_id');
        // await removeChildItem({
        //   model: Language,
        //   records: _.filter(oldLanguageMap, item => (_.has(item, '_id') ? newLanguageMapIdList.indexOf(_.get(item, '_id')) === -1 : true)),
        // });
        // const saveListItemResults = await updateChildItem({ model: ResourceListItem, records: list });
        // // ? Remove the ListItem if removed
        // const newListIdList = _.map(_.filter(list, item => _.has(item, '_id')), '_id');
        // await removeChildItem({
        //   model: ResourceListItem,
        //   records: _.filter(oldList, item => (_.has(item, '_id') ? newListIdList.indexOf(_.get(item, '_id')) === -1 : true)),
        // });
        // saveResult = await ResourceList.findOneAndUpdate({ _id: resourceId }, {
        //   ..._.omit(resourceData, ['_id']),
        //   key,
        //   title,
        //   referenceId,
        //   list: saveListItemResults,
        //   languageMap: saveLanguageResults,
        //   updatedAt: moment(),
        // }, findOneAndUpdateOptions).lean().exec();
        break;
      }
      case CASCADE: {
        // ? 1. We update the languageMap
        const {
          languageMap, list, key, title, referenceId, mapping,
        } = data;
        // ? 2. Find the Resource
        const resourceData = await ResourceCascade.findOne({ _id: resourceId }).lean().exec();
        if (!resourceData) {
          throw new Error(JSON.stringify({
            type: 'badRequest',
            message: 'Cannot find a record using the resourceId provided',
          }));
        }
        const saveLanguageResults = await updateChildItem({ model: Language, records: languageMap, debug: 'lang' });
        // ? 3. We remove the existing list
        await removeChildItem({
          model: ResourceList,
          records: resourceData.list,
        });
        // ? 4. We update the list
        const saveListResult = await aigle.map(list, async (listItem) => {
          const { _id: listId, ...otherParams } = listItem;
          const subResult = await updateTypeListResource(listId, otherParams);
          return subResult;
        });
        // ? 5. Save it
        saveResult = await ResourceCascade.findOneAndUpdate({ _id: resourceId }, {
          ..._.omit(resourceData, ['_id']),
          key,
          title,
          referenceId,
          mapping,
          list: saveListResult,
          languageMap: saveLanguageResults,
          updatedAt: moment(),
        }, findOneAndUpdateOptions).lean().exec();
        break;
      }
      /* Add your case here */
      default: {
        responseUtil.generalBadRequestResponse(req, res, next, 'Invalid type provided');
        return;
      }
    }
    responseUtil.generalSuccessResponse(req, res, next, { ...saveResult, type });
  } catch (err) {
    const isJSON = (str) => {
      try {
        JSON.parse(str);
        return true;
      } catch (e) {
        return false;
      }
    };
    const { type: errorType, message: errorMessage } = isJSON(err.message) ? JSON.parse(err.message) : err;
    switch (errorType) {
      case 'badRequest':
        responseUtil.generalBadRequestResponse(req, res, next, errorMessage);
        break;
      default:
        responseUtil.generalInternalServerErrorResponse(req, res, next, err);
        break;
    }
  }
};
