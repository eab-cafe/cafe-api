const faker = require('faker');

const log4jUtil = require('../../app/v1/utils/log4j.util');

describe('Log4js Util', () => {
  it('[function]log("all", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('all', fakeInfo);
  });

  it('[function]log("all", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('all', fakeInfo, faker.random.number());
  });

  it('[function]log("trace", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('trace', fakeInfo);
  });

  it('[function]log("trace", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('trace', fakeInfo, faker.random.number());
  });

  it('[function]log("debug", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('debug', fakeInfo);
  });

  it('[function]log("debug", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('debug', fakeInfo, faker.random.number());
  });

  it('[function]log("info", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('info', fakeInfo);
  });

  it('[function]log("info", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('info', fakeInfo, faker.random.number());
  });

  it('[function]log("warn", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('warn', fakeInfo);
  });

  it('[function]log("warn", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('warn', fakeInfo, faker.random.number());
  });

  it('[function]log("error", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('error', fakeInfo);
  });

  it('[function]log("error", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('error', fakeInfo, faker.random.number());
  });

  it('[function]log("fatal", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('fatal', fakeInfo);
  });

  it('[function]log("fatal", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('fatal', fakeInfo, faker.random.number());
  });

  it('[function]log("mark", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('mark', fakeInfo);
  });

  it('[function]log("mark", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('mark', fakeInfo, faker.random.number());
  });

  it('[function]log("OFF", info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('off', fakeInfo);
  });

  it('[function]log("OFF", info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log('off', fakeInfo, faker.random.number());
  });

  it('[function]log(ELSE CASE, info) [cause] Success', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log(faker.random.word(), fakeInfo);
  });

  it('[function]log(ELSE CASE, info) [cause] Success with uuid', () => {
    const fakeInfo = faker.random.words();
    log4jUtil.log(faker.random.word(), fakeInfo, faker.random.number());
  });
});
