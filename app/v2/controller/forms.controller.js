/**
 * @module controller/form
 */
// const _ = require('lodash');
// const moment = require('moment');

const formModel = require('../models/form');
// const Language = require('../models/language');
// const FormPublish = require('../models/formPublish');
// const Page = require('../models/page');
// const Section = require('../models/section');
// const Question = require('../models/question');
// const MinIoMapping = require('../models/minIoMapping');
// const FormDesignTheme = require('../models/formDesignTheme');


// const validateUtil = require('../utils/validator.util');
const responseUtil = require('../utils/response.util');
// const log4jUtil = require('../utils/log4j.util');
// const formUtil = require('../utils/form.util');
// const commonUtil = require('../utils/common.util');

function FormWrapper(FormSchema) {
  this.model = new FormSchema();

  this.getForms = async () => this.model.getForms();
  this.getForm = async formId => this.model.getFormById(formId);
}

const formInstance = new FormWrapper(formModel);

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get a set of form data
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form}
 */
module.exports.getForms = async (req, res, next) => {
  try {
    const results = await formInstance.getForms();
    responseUtil.generalSuccessResponse(req, res, next, results);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get form data
 * @param {object} req - express request param
 * @param {object} req.params - params of express request param
 * @param {string} req.params.formId - formId from url
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/Form/get_form__formId_}
 */
module.exports.getForm = async (req, res, next) => {
  try {
    const { params: { formId } } = req;
    const results = await formInstance.getForm(
      formId,
    );
    responseUtil.generalSuccessResponse(req, res, next, results);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};

// module.exports = formInstance;
