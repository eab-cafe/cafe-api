/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable func-names */

const _ = require('lodash');
const {
  TEXT_INPUT,
  SELECTOR,
  DROP_DOWN,
  TOGGLE,
  DATE,
  SLIDER,
  CALC_INPUT,
  BASIC,
  ADVANCED,
  PLAIN_TEXT,
  HANDWRITING,
  SLIDER_CV,
  CONDITIONAL_CHOICE,
  TABLE,
} = require('../constants/questionType');
const {
  ARRAY, ARRAYOBJECT, OBJECT, TEXT, ARRAYQUESTION,
} = require('../constants/answerType');

const QuestionAnswer = function ({
  questionType,
  question,
  value = {},
  type = null,
  formatProps = {},
  styleProps = {},
  optionalProps = null,
}) {
// class QuestionAnswer {
//   constructor({
//     questionType, // pass the questionType to here for validation
//     question, // pass the original question to here for rewriting question.answers at final stage
//     value = {}, // value object, contains TEXT, ARRAY, OBJECT properties respectively
//     type = null, // answerType, if the question has answerType indentification pass it here
//     formatProps = {}, // formatting properties, including validation, customization of error message etc
//     styleProps = {}, // styling properties, reserved for CSS customization of each answers
//     optionalProps = null, // optional properties, only applicable if there are "optional" fields
//   }) {
  this._value = value;
  this._question = question;
  //   this._questionType = validateQuestionType(questionType);
  this._questionType = questionType;
  this._type = type;
  this._formatProps = formatProps;
  this._styleProps = styleProps;
  this._optionalProps = optionalProps;
  //   }

  // update = ({ ...props }) => {
  //   this._value = Object.assign({}, this._value, _.get(props, "value", {}));
  //   this._type = _.get(props, "type", this._type);
  //   this._formatProps = Object.assign(
  //     {},
  //     this._formatProps,
  //     _.get(props, "formatProps", {})
  //   );
  //   this._styleProps = Object.assign(
  //     {},
  //     this.styleProps,
  //     _.get(props, "styleProps")
  //   );
  //   this._optionalProps = Object.assign(
  //     {},
  //     this._optionalProps,
  //     _.get(props, "optionalProps")
  //   );
  //   this.WriteToQuestion();
  // };
  // #region value setter getter

  this.questionType = () => this._questionType;

  this.value = () => {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
        return this.valueArray();
      case SLIDER:
        switch (this._type) {
          case SLIDER_CV:
            return this.valueArray();
          default:
            return this.valueObject();
        }
      case CALC_INPUT:
        switch (this._type) {
          case BASIC:
            return this.valueArrayObject();
          case ADVANCED:
            return this.valueObject();
          default:
            return this.valueArrayObject();
        }
      case DATE:
        return this.valueArrayObject();
      case TABLE:
        return this.valueArrayQuestion();
      case PLAIN_TEXT:
      case TEXT_INPUT:
      case HANDWRITING:
        return this.valueObject();
      case CONDITIONAL_CHOICE:
        return this.valueArrayObject();
      default:
        return this.valueText();
    }
  };
  this.valueType = () => {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
        return ARRAY;
      case SLIDER:
        switch (this._type) {
          case SLIDER_CV:
            return ARRAY;
          default:
            return OBJECT;
        }
      case CALC_INPUT:
        switch (this._type) {
          case BASIC:
            return ARRAYOBJECT;
          case ADVANCED:
            return OBJECT;
          default:
            return ARRAYOBJECT;
        }
      case CONDITIONAL_CHOICE:
      case DATE:
        return ARRAYOBJECT;
      case PLAIN_TEXT:
      case TEXT_INPUT:
      case HANDWRITING:
        return OBJECT;
      default:
        return TEXT;
    }
  };

  // get values() {
  //   return this._value;
  // }
  this.valueText = () => this._value[TEXT];
  this.valueArray = () => this._value[ARRAY];
  this.valueObject = () => this._value[OBJECT];
  this.valueArrayObject = () => this._value[ARRAYOBJECT];
  this.valueArrayQuestion = () => this._value[ARRAYQUESTION];
  // set value({ ...val }) {
  //   if (
  //     !_.has(val, TEXT) &&
  //     !_.has(val, ARRAY) &&
  //     !_.has(val, OBJECT) &&
  //     !_.has(val, ARRAYOBJECT)
  //   ) {
  //     throwErr(`questionUtils:: QuestionAnswer:: valueType not exist`);
  //   }
  //   this._value = {
  //     ...this._value,
  //     ...val
  //   };
  //   this.WriteToQuestion();
  // }
  // set valueText(val) {
  //   this.value = {
  //     [TEXT]: val
  //   };
  // }
  // set valueArray(val) {
  //   this.value = {
  //     [ARRAY]: val
  //   };
  // }
  // set valueObject(val) {
  //   this.value = {
  //     [OBJECT]: val
  //   };
  // }
  // set valueArrayObject(val) {
  //   this.value = {
  //     [ARRAYOBJECT]: val
  //   };
  // }
  // #region type setter getter
  // get type() {
  //   // validation, fallback
  //   switch (this._questionType) {
  //     case TEXT_INPUT:
  //       return [INPUT_LINE, INPUT_FIELD].indexOf(this._type) > -1
  //         ? this._type
  //         : INPUT_LINE;
  //     case SELECTOR:
  //       return [SINGLE_ANSWER, MUITIPLE_ANSWER].indexOf(this._type) > -1
  //         ? this._type
  //         : SINGLE_ANSWER;
  //     case DROP_DOWN:
  //       return [CUSTOM, COUNTRY].indexOf(this._type) > -1 ? this._type : CUSTOM;
  //     case DATE:
  //       return [INPUT_DATE, INPUT_TIME, INPUT_DATE_N_TIME].indexOf(this._type) >
  //         -1
  //         ? this._type
  //         : INPUT_DATE;
  //     case TOGGLE:
  //       return [INPUT_TWOSTEP, INPUT_THREESTEP].indexOf(this._type) > -1
  //         ? this._type
  //         : INPUT_TWOSTEP;
  //     case CALC_INPUT:
  //       return [BASIC, ADVANCED].indexOf(this._type) > -1 ? this._type : BASIC;
  //     case SLIDER:
  //       return [
  //         SLIDER_BASIC,
  //         SLIDER_BASIC_L,
  //         SLIDER_BASIC_LV,
  //         SLIDER_SCALES,
  //         SLIDER_CV
  //       ].indexOf(this._type) > -1
  //         ? this._type
  //         : SLIDER_BASIC;
  //   }
  //   return this._type;
  // }
  // set type(val) {
  //   this._type = val;
  //   this.WriteToQuestion();
  // }
  // // #endregion
  // // #region formatProps setter getter
  // get formatProps() {
  //   return this._formatProps;
  // }

  // set formatProps({ ...newFormatProps }) {
  //   this._formatProps = {
  //     ...this.formatProps,
  //     ...newFormatProps
  //   };
  //   this.WriteToQuestion();
  // }
  // // #endregion

  // // #region styleProps setter getter
  // get styleProps() {
  //   return this._styleProps;
  // }
  // set styleProps({ ...newStyleProps }) {
  //   this._styleProps = {
  //     ...this.styleProps,
  //     ...newStyleProps
  //   };
  //   this.WriteToQuestion();
  // }
  // // #endregion

  // // #region optionalProps setter getter
  this.optionalProps = () => this._optionalProps;
  // set optionalProps({ ...newOptionalProps }) {
  //   this._optionalProps = {
  //     ...this._optionalProps,
  //     ...newOptionalProps
  //   };
  //   this.WriteToQuestion();
  // }
  // // #endregion

  // WriteToQuestion = () => {
  //   if (this._question) {
  //     this._question.answers = {
  //       value: this._value,
  //       type: this._type,
  //       formatProps: this._formatProps,
  //       styleProps: this._styleProps,
  //       optionalProps: this._optionalProps
  //     };
  //     // if (this._questionType === TEXT_INPUT) {
  //     // console.warn("WriteToQuestion:: this._question.answers");
  //     // console.log(this._question.answers);
  //     // }
  //   }
  // };
};

const AnswerMapper = function ({
  questionType,
  type = null,
  value = null,
  optionalValue = null,
}) {
  this._questionType = questionType;
  this._type = type;
  this._value = value;
  this._optionalValue = optionalValue;
  this._optionalValueType = TEXT;

  this.valueType = () => {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case SLIDER:
        return ARRAY;
      case CALC_INPUT:
      case DATE:
        return ARRAYOBJECT;
      case PLAIN_TEXT:
      case HANDWRITING:
        return OBJECT;
      case TEXT_INPUT:
      default:
        return TEXT;
    }
  };

  this.isDisplayId = () => {
    switch (this.valueType()) {
      case ARRAY:
      case ARRAYOBJECT:
        return true;
      case OBJECT:
      case TEXT:
      default:
        return false;
    }
  };

  this.isWritable = () => {
    switch (this._questionType) {
      case DROP_DOWN:
      case SELECTOR:
      case TOGGLE:
      case DATE:
      case TEXT_INPUT:
        return true;
      case CALC_INPUT:
      case PLAIN_TEXT:
      case HANDWRITING:
      default:
        return false;
    }
  };

  this.setValue = (val) => {
    this._value = val;
  };

  this.setOptionalValue = (val) => {
    this._optionalValue = val;
  };

  this.ToMap = () => {
    let result = {
      value: this._value,
      valueType: this.valueType(),
      type: this._type,
      isDisplayId: this.isDisplayId(),
      isWritable: this.isWritable(),
    };
    if (!_.isEmpty(this._optionalValue)) {
      result = {
        ...result,
        optionalValue: this._optionalValue,
        optionalValueType: this._optionalValueType,
      };
    }
    return result;
  };
};

module.exports.AnswerMapper = AnswerMapper;
module.exports.QuestionAnswer = QuestionAnswer;
