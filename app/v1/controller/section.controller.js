/**
 * @module controller/section
 */
const Page = require('../models/page');
const Section = require('../models/section');

const responseUtil = require('../utils/response.util');


/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Save section information to mongodb
 * @param {object} req - express request param
 * @param {object} req.body - express request body
 * @param {string} req.body.pageId - page id which the section belongs to
 * @param {string} req.body.index - index of section of page
 * @param {string} req.body.title - section title
 * @param {string=} req.body.description - section description
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns new section record id from mongodb
 */
module.exports.saveSection = async (req, res, next) => {
  if (req.body.pageId && req.body.index && req.body.title) {
    const page = await Page.findOne({ _id: req.body.pageId });
    if (page) {
      if (/[0-9]+/.test(req.body.index) && req.body.index >= 0 && req.body.index <= page.sections.length) {
        const newSection = new Section({
          title: req.body.title,
          description: req.body.description ? req.body.description : '',
          questions: [],
          designSetting: req.body.designSetting ? req.body.designSetting : {},
          columnCount: req.body.columnCount ? req.body.columnCount : undefined,
          visibilityRule: req.body.visibilityRule ? req.body.visibilityRule : [],
          referenceId: req.body.referenceId ? req.body.referenceId : '',
        });
        const id = '_id';
        const newSectionResult = await newSection.save();
        page.sections.splice(req.body.index, 0, newSectionResult[id]);
        await Page.findOneAndUpdate({
          _id: req.body.pageId,
        }, {
          sections: page.sections,
        }, {
          new: true,
        });
        responseUtil.generalSuccessResponse(req, res, next, {
          _id: newSectionResult[id],
        });
      } else {
        responseUtil.generalBadRequestResponse(req, res, next, 'Index parameter error. It should be number and >=0 and <= form.pages.length');
      }
    } else {
      responseUtil.generalBadRequestResponse(req, res, next, `Page ${req.body.pageId} not exists`);
    }
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, 'pageId, index, title are required');
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Get section information from mongodb
 * @param {object} req - express request param
 * @param {object} req.params - express param body
 * @param {string} req.params.sectionId - section id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Section info
 */
module.exports.getSection = async (req, res, next) => {
  const sectionResult = await Section.findOne({ _id: req.params.sectionId });
  if (sectionResult) {
    responseUtil.generalSuccessResponse(req, res, next, sectionResult);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Section ${req.params.sectionId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Update section information
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.sectionId - section id pass from client side
 * @param {object} req.body - express request body
 * @param {string=} req.body.title - new section title
 * @param {string=} req.body.description - new section description
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns New section info
 */
module.exports.updateSection = async (req, res, next) => {
  const result = await Section.findOne({ _id: req.params.sectionId });
  if (result && req.body === {}) {
    responseUtil.generalSuccessResponse(req, res, next, result);
  } else if (result) {
    const newSectionData = {};
    if (req.body.title) {
      newSectionData.title = req.body.title;
    }
    if (req.body.description) {
      newSectionData.description = req.body.description;
    }
    const newSection = await Section.findOneAndUpdate({
      _id: req.params.sectionId,
    }, newSectionData, {
      new: true,
    });
    responseUtil.generalSuccessResponse(req, res, next, newSection);
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Section ${req.params.sectionId} not exists.`);
  }
};

/**
 * @author Kevin Liang <kevin.liang@eabsystems.com>
 * @function
 * @description Delete section
 * @param {object} req - express request param
 * @param {object} req.params - express request param
 * @param {string} req.params.sectionId - section id pass from client side
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @returns Deleted section id
 */
module.exports.deleteSection = async (req, res, next) => {
  const result = await Section.findOne({ _id: req.params.sectionId });
  if (result) {
    await Section.findByIdAndDelete({ _id: req.params.sectionId });
    responseUtil.generalSuccessResponse(req, res, next, {
      _id: req.params.sectionId,
    });
  } else {
    responseUtil.generalBadRequestResponse(req, res, next, `Section ${req.params.sectionId} not exists.`);
  }
};
