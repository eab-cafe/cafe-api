const mongoose = require('mongoose');

const SectionSchema = new mongoose.Schema({
  displayId: {
    type: String,
  },
  title: {
    type: String,
    // required: true,
  },
  description: {
    type: String,
  },
  questions: {
    type: Array, // [mongoose.Schema.Types.ObjectId] // Don't know what happen
    ref: 'Question',
    default: [],
  },
  designSetting: {
    type: Object,
    default: {},
  },
  visibilityRule: {
    type: Array,
    default: [],
  },
  columnCount: {
    type: Number,
  },
  referenceId: {
    type: String,
    default: '',
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
}, { minimize: false });

module.exports = mongoose.model('Section', SectionSchema);
