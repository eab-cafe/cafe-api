const express = require('express');

const router = express.Router();

const sectionController = require('../controller/section.controller');

router.post('/', (req, res, next) => {
  sectionController.saveSection(req, res, next);
});

router.get('/:sectionId', (req, res, next) => {
  sectionController.getSection(req, res, next);
});

router.put('/:sectionId', (req, res, next) => {
  sectionController.updateSection(req, res, next);
});

router.delete('/:sectionId', (req, res, next) => {
  sectionController.deleteSection(req, res, next);
});

module.exports = router;
