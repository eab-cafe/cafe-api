/**
 * @module controller/form
 */
const FormDesignTheme = require('../models/formDesignTheme');
const responseUtil = require('../utils/response.util');
/**
 * @author xue hua <xue.hua@eabsystems.com>
 * @function
 * @description Get a set of form data
 * @param {object} req - express request param
 * @param {object} res - express response param
 * @param {object} next - express next middleware function
 * @see {@link http://eab-cafe.eabgitlab.com/cafe-api/docs/index.html#/FormDesignTheme/get_formDesignTheme}
 */
module.exports.getDesignThemes = async (req, res, next) => {
  try {
    const results = await FormDesignTheme.find().catch(err => ({ error: err }));
    responseUtil.generalSuccessResponse(req, res, next, results);
  } catch (err) {
    responseUtil.generalInternalServerErrorResponse(req, res, next, err);
  }
};
