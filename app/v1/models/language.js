const mongoose = require('mongoose');

// If there are formId, language belongs to form.
// If there are userId, language belongs to users.
// ? Added refId for replacing the formId and userId,
// ? as an unique reference Key for all other Ids.

// ! Gonna move formId && useId to refId and specify the idType in refType.
const LanguageModel = new mongoose.Schema({
  formId: {
    type: String,
  },
  userId: {
    type: String,
  },
  refId: {
    type: String,
  },
  refType: {
    type: String,
  },
  lang: {
    type: String,
    required: true,
  },
  set: {
    type: Object,
  },
}, { minimize: true });

module.exports = mongoose.model('Language', LanguageModel);
