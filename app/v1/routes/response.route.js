const express = require('express');

const router = express.Router();

const responseController = require('../controller/response.controller');

router.post('/', (req, res, next) => {
  responseController.saveResponse(req, res, next);
});

router.get('/:responseId', (req, res, next) => {
  responseController.getResponse(req, res, next);
});

router.delete('/:responseId', (req, res, next) => {
  responseController.deleteResponse(req, res, next);
});

router.post('/importData', (req, res, next) => {
  responseController.saveImportData(req, res, next);
});

router.put('/:responseId/importData', (req, res, next) => {
  responseController.updateImportData(req, res, next);
});

router.get('/list/:publishFormId', (req, res, next) => {
  responseController.getResponseList(req, res, next);
});

router.post('/getResponseDataToMas', (req, res, next) => {
  responseController.getResponseDataToMas(req, res, next);
});

router.post('/getResponseStatus', (req, res, next) => {
  responseController.getResponseStatus(req, res, next);
});

router.post('/getResponseDataToDP', (req, res, next) => {
  responseController.getResponseDataToDP(req, res, next);
});

module.exports = router;
