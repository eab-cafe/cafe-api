const express = require('express');

const router = express.Router();

const responseUtil = require('../utils/response.util');

router.get('/', (req, res, next) => {
  responseUtil.generalSuccessResponse(req, res, next, 'test Hello World v2');
});

module.exports = router;
