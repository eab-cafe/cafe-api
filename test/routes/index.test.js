const request = require('supertest');
const mongoose = require('mongoose');

const app = require('../../app');

describe('Router', () => {
  it('[router][GET] / [cause] Success', (done) => {
    request(app)
      .get('/')
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text))
          .toEqual({
            status: 'error',
            message: '',
          });
        done();
      });
  });

  it('[router][GET] /get200 [cause] Success', (done) => {
    request(app)
      .get('/get200')
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        expect(JSON.parse(response.text))
          .toEqual({
            status: 'success',
            result: '',
          });
        done();
      });
  });

  it('[router][GET] /get404 [cause] Status 404', (done) => {
    request(app).get('/get404')
      .then((response) => {
        expect(response.status)
          .toBe(404);
        done();
      });
  });

  it('[router][GET] /get500 [cause] Status 500', (done) => {
    request(app)
      .get('/get500')
      .then((response) => {
        expect(response.status)
          .toBe(500);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        done();
      });
  });

  it('[router][GET] /get400 [cause]Status 400', (done) => {
    request(app)
      .get('/get400')
      .then((response) => {
        expect(response.status)
          .toBe(400);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        done();
      });
  });

  it('[router][GET]custom response status [cause]Status 234', (done) => {
    request(app)
      .get('/getCustomResponse/234')
      .then((response) => {
        expect(response.status)
          .toBe(234);
        expect(response.header['content-type'])
          .toBe('application/json; charset=utf-8');
        done();
      });
  });
});

afterAll(async (done) => {
  mongoose.connection.close();
  done();
});
